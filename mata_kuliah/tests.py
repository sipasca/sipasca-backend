
from urllib import response
from django.test import TestCase

from mata_kuliah.views import upload_form_persyaratan
from .db import create_mata_kuliah, create_form_mk, create_form_persyaratan
from profiles.db import create_mahasiswa
from unittest import mock
import json

send_type = "application/json"
url_tambah = '/tambahMataKuliah'
url_get_dospem_mk = '/getDosenPembimbingMK/CSGE802097/M9636831678/'
url_insert = '/insertMKMahasiswa/'
url_request_dospem = '/insertRequestDosenPembimbing/'
url_request_migrasi = "/migrasi"
url_accept_migrasi = "/acceptMigrasi"
url_reject_migrasi = "/rejectMigrasi"
url_detail_migrasi = "/getDetailMigrasi/CSGE802098_2022-2/M9636831678/"

dummy_form_jenis_persyaratan = "Tidak Ada"

dummy_form = {
    'jenis_form': 'dummy',
    'jenis_persyaratan': 'Prasyarat'
}

dummy_mk = {
    'mataKuliahKode':'dummy',
    'mataKuliahPeriode':'dummy',
    'mataKuliahNama':'dummy',
    'mataKuliahMahasiswa':30,
    'mataKuliahKredit': 6,
    'mataKuliahKurikulum':'dummy',
    'mataKuliahTugasAkhir': 'yes'
}
dummy_mk_2 = {
    'mataKuliahKode':'dummy_dummy',
    'mataKuliahPeriode':'dummy',
    'mataKuliahNama':'dummy',
    'mataKuliahMahasiswa':30,
    'mataKuliahKredit': 6,
    'mataKuliahKurikulum':'dummy',
    'mataKuliahTugasAkhir': 'yes'
}


# Create your tests here.
class MataKuliahTest(TestCase) :
    def test_get_mk_all_successfully(self):
        response = self.client.get('/mata_kuliah/M9636831678/')
        self.assertEqual(response.status_code, 200)

    def test_get_mk_can_be_requested_successfully(self):
        response = self.client.get('/mata_kuliah_can_be_requested/M9636831678/')
        self.assertEqual(response.status_code, 200)

    def test_get_mk_tersedia_successfully(self):
        response = self.client.get('/daftar_mata_kuliah/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_mk_tersedia_mahasiswa_successfully(self):
        response = self.client.get('/daftar_mata_kuliah_mahasiswa/M9636831678')
        self.assertEqual(response.status_code, 200)

    def test_tambah_mata_kuliah_successfully(self):
        data = {
         'kode' : 'kode',
         'periode' : 'periode',
         'nama' : 'nama',
         'jumlah_mahasiswa' : 1,
         'kredit' : 1,
         'kurikulum' : 'kurikulum',
         'tugas_akhir' : 'true',
         'prasyarat' : 'prasyarat'
         }

        response = self.client.post(url_tambah, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_tambah_mata_kuliah_failed(self):
        create_mata_kuliah(dummy_mk_2)

        data = {
         'kode' : 'dummy',
         'periode' : 'dummy',
         'nama' : 'nama',
         'jumlah_mahasiswa' : 1,
         'kredit' : 1,
         'kurikulum' : 'kurikulum',
         'tugas_akhir' : 'true',
         'prasyarat' : 'prasyarat'
         }
        
        response = self.client.post(url_tambah, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'FAILED')
        self.assertEqual(response.status_code, 200)

    def test_exists_halaman_form_persyaratan_successfully(self):
        create_mata_kuliah(dummy_mk)

        response = self.client.get('/formPersyaratan/dummy')
        self.assertEqual(response.status_code, 200)
    
    def test_tambah_form_persyaratan_successfully(self):
        create_mata_kuliah(dummy_mk)

        response = self.client.post('/tambahFormPersyaratan/dummy', json.dumps(dummy_form), content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_tambah_form_persyaratan_failed(self):
        create_mata_kuliah(dummy_mk)
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        response = self.client.post('/tambahFormPersyaratan/dummy', json.dumps(dummy_form), content_type=send_type)
        self.assertEqual(response.status_code, 200)
        
    def test_exists_halaman_detail_form_persyaratan_successfully(self):
        create_mata_kuliah(dummy_mk)
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)

        response = self.client.get('/detailFormPersyaratan/dummy/dummy')
        self.assertEqual(response.status_code, 200)

    def test_exists_detail_mk_successfully(self):
        create_mata_kuliah(dummy_mk)
        response = self.client.get('/detailMataKuliah/dummy/')
        self.assertEqual(response.status_code, 200)

    def test_successfully_get_dosen_pembimbing_per_mk(self):
        response = self.client.get(url_get_dospem_mk, content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('dospem_list')[0][1], 'dosen1')
    
    def test_mendaftarkan_mk_mahasiswa_successfully(self):
        data = {
         'id_mk' : 'CSGE802099',
         'id_mahasiswa' : 'M9636831678'}
        response = self.client.post(url_insert, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    @mock.patch('googleapiclient.discovery.Resource')
    def test_insert_request_dosen_pembimbing_success(self, mock_build):
        data = {
            'id_mk' : 'CSGE802097',
            'id_dosen' : 'D1234567890',
            'id_mahasiswa' : 'M1234567890',
            'judul' : 'judul',
            'deskripsi' : 'deskripsi',
            'motivasi' : 'motivasi',
            'batas_waktu' : '2022-3-3',
            'pesan' : 'pesan',
            'id_topik':'28'
         }
        response = self.client.post(url_request_dospem, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)
    
    @mock.patch('googleapiclient.discovery.Resource')
    def test_insert_request_dosen_pembimbing_with_null_topik(self, mock_build):
        data = {
            'id_mk' : 'CSGE802097',
            'id_dosen' : 'D1234567890',
            'id_mahasiswa' : 'M1234567890',
            'judul' : 'judul',
            'deskripsi' : 'deskripsi',
            'motivasi' : 'motivasi',
            'batas_waktu' : '2022-3-3',
            'pesan' : 'pesan',
            'id_topik': ''
         }
        response = self.client.post(url_request_dospem, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)
    

    @mock.patch('googleapiclient.discovery.Resource')
    def test_send_mail_bapak_dosen(self, mock_build):
        data = {
            'id_mk' : 'CSGE802097',
            'id_dosen' : 'D5555555555',
            'id_mahasiswa' : 'M1234567890',
            'judul' : 'judul topik',
            'deskripsi' : 'deskripsi topik',
            'motivasi' : 'motivasi topik',
            'batas_waktu' : '2022-3-13',
            'pesan' : 'pesan tambahan',
            'id_topik':'28'
        }
        response = self.client.post(url_request_dospem, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_halaman_upload_form_persyaratan_exists(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'dummy',
            'nama_file' : 'dummy',
            'jenis_form' : 'dummy'
        }
        response = self.client.post('/uploadFormPersyaratan/',json.dumps(data), content_type=send_type)
        self.assertEqual(response.status_code, 200)
    
    def test_upload_form_persyaratan_success(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'dummy',
            'nama_file' : 'dummy',
            'jenis_form' : 'dummy'
        }
        response = self.client.post('/uploadFormPersyaratan/',json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        
    def test_halaman_hapus_form_persyaratan_exists(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'dummy',
            'nama_file' : 'dummy',
            'jenis_form' : 'dummy'
        }
        create_form_persyaratan(data)
        response = self.client.get('/hapusFormPersyaratan/dummy/dummy/dummy', content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_hapus_form_persyaratan_success(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'dummy',
            'nama_file' : 'dummy',
            'jenis_form' : 'dummy'
        }
        create_form_persyaratan(data)
        response = self.client.get('/hapusFormPersyaratan/dummy/dummy/dummy', content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    def test_halaman_file_form_persyaratan_exists(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'dummy',
            'nama_file' : 'dummy',
            'jenis_form' : 'dummy',
        }
        create_form_persyaratan(data)
        response = self.client.get('/fileFormPersyaratan/dummy/dummy/dummy', content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_file_form_persyaratan_success(self):
        create_mata_kuliah(dummy_mk)
        create_mahasiswa("dummy","dummy","dummy","dummy")
        create_form_mk("dummy","dummy", dummy_form_jenis_persyaratan)
        data = {
            'id_mk' : 'dummy',
            'id_mahasiswa' : 'dummy',
            'url' : 'urldummy',
            'nama_file' : 'namadummy',
            'jenis_form' : 'dummy'
        }
        create_form_persyaratan(data)
        response = self.client.get('/fileFormPersyaratan/dummy/dummy/dummy', content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('detail_file')[0][0], 'urldummy')
        self.assertEqual(response_data.get('detail_file')[0][1], 'namadummy')

    def test_request_migrasi_successfully_with_periode_baru_in_different_year(self):
        data = {
         'id_mk' : 'CSGE801092',
         'id_mahasiswa' : 'M9636831678',
         'periode_lama' : '2022-2',
         }

        response = self.client.post(url_request_migrasi, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_get_all_request_migrasi_successfully(self):
        response = self.client.get('/getAllRequestMigrasi/CSGE802098_2022-2')
        self.assertEqual(response.status_code, 200)
    
    def test_successfully_get_detail_migrasi_per_mahasiswa_dan_mk(self):
        response = self.client.get(url_detail_migrasi, content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_accept_migrasi_successfully(self):
        data = {
         'id_mk' : 'CSGE802098_2022-2',
         'id_mahasiswa' : 'M9636831678',
        }
        response = self.client.post(url_accept_migrasi, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_accept_migrasi_successfully_mahasiswa_with_empty_mkfile_and_mkdosen(self):
        data = {
         'id_mk' : 'CSGE802100_2022-2',
         'id_mahasiswa' : 'M9636831678',
        }
        response = self.client.post(url_accept_migrasi, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_reject_migrasi_successfully(self):
        data = {
         'id_mk' : 'CSGE802101_2022-2',
         'id_mahasiswa' : 'M9636831678',
        }
        response = self.client.post(url_reject_migrasi, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_get_term_aktif(self):
        response = self.client.get('/getTermAktif/')
        self.assertEqual(response.status_code,200)