
from unicodedata import name
from django.urls import path
from . import views

app_name = 'mata_kuliah'

urlpatterns =[
    path('mata_kuliah/<str:id_mahasiswa>/', views.mata_kuliah_pages, name="mata_kuliah"),
    path('mata_kuliah_can_be_requested/<str:id_mahasiswa>/', views.mata_kuliah_can_be_requested, name="mata_kuliah_can_be_requested"),
    path('daftar_mata_kuliah/', views.daftar_mk_pages, name="daftar_mata_kuliah"),
    path('daftar_mata_kuliah_mahasiswa/<str:id_mahasiswa>', views.daftar_mk_pages_mahasiswa, name="daftar_mata_kuliah_mahasiswa"),
    path('tambahMataKuliah', views.tambah_mata_kuliah, name="tambah_mata_kuliah"),
    path('getDosenPembimbingMK/<str:mk_id>/<str:mahasiswa_id>/', views.dosen_pembimbing_mk, name="dosen_pembimbing_mk"),
    path('formPersyaratan/<str:id_mk>', views.form_persyaratan, name ="form_persyaratan"),
    path('tambahFormPersyaratan/<str:id_mk>', views.tambah_form_persyaratan, name ="tambah_form_persyaratan"),
    path('detailFormPersyaratan/<str:id_mk>/<str:jenis_form>',views.detail_form_persyaratan, name="detail_form_persyaratan"),
    path('detailMataKuliah/<str:id_mk>/', views.get_detail_mk, name="get_detail_mk"),
    path('insertMKMahasiswa/', views.mendaftarkan_mk_mahasiswa, name="insertMKMahasiswa"),
    path('insertRequestDosenPembimbing/', views.handle_request_dospem, name="insertRequestDosenPembimbing"),
    path('uploadFormPersyaratan/', views.upload_form_persyaratan, name ="upload_form_persyaratan"),
    path('hapusFormPersyaratan/<str:id_mk>/<str:id_mahasiswa>/<str:jenis_form>',views.hapus_form_persyaratan, name="hapus_form_persyaratan"),
    path('fileFormPersyaratan/<str:id_mk>/<str:id_mahasiswa>/<str:jenis_form>',views.file_form_persyaratan, name="file_form_persyaratan"),
    path('migrasi',views.request_migrasi,name="request_migrasi"),
    path('acceptMigrasi',views.accept_migrasi,name="accept_migrasi"),
    path('rejectMigrasi',views.reject_migrasi,name="reject_migrasi"),
    path('getAllRequestMigrasi/<str:id_mk>',views.get_all_request_migrasi,name="get_all_request_migrasi"),
    path('getDetailMigrasi/<str:id_mk>/<str:id_mahasiswa>/',views.get_detail_migrasi,name="get_detail_migrasi"),
    path('getTermAktif/',views.get_term_aktif,name="get_term_aktif"),
]