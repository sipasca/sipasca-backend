
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_mk_all(id_mahasiswa) :
     ''' Mengembalikan nama dan periode dari mata_kuliah'''
     with connection.cursor() as cursor:
        cursor.execute(
        """SELECT mk.nama, mk.periode, mk.kode 
        FROM MATA_KULIAH as mk, MK_MAHASISWA as mkm 
        WHERE mk.kode = mkm.id_mk AND mkm.id_mahasiswa = %s;
        """, [id_mahasiswa]
        )
        rows = namedtuplefetchall(cursor)
        return rows

def get_mk_can_be_requested(id_mahasiswa) :
     ''' Mengembalikan nama, periode, dan kode dari mata_kuliah yang masih bisa direquest'''
     with connection.cursor() as cursor:
        cursor.execute(
        """SELECT mk.nama, mk.periode, mk.kode FROM MATA_KULIAH as mk, MK_MAHASISWA as mkm, TERM_AKTIF as t 
        WHERE mk.kode = mkm.id_mk AND mkm.id_mahasiswa = %s AND mk.kode NOT IN 
        (SELECT id_mk FROM MK_MAHASISWA_DOSEN WHERE id_mahasiswa = %s) AND mk.periode = t.term
        """, [id_mahasiswa,id_mahasiswa]
        )
        rows = namedtuplefetchall(cursor)
        return rows

def get_mk_tersedia() :
    '''Mengembalikan kode,periode,nama,jumlah_mahasiswa,kredit,kurikulum,tugas_akhir'''
    with connection.cursor() as cursor:
        cursor.execute(
         """SELECT * FROM MATA_KULIAH;
         """)
        rows = namedtuplefetchall(cursor)
        return rows 

def get_mk_tersedia_mahasiswa(id_mahasiswa) :
    '''Mengembalikan kode,periode,nama,jumlah_mahasiswa,kredit,kurikulum,tugas_akhir'''
    with connection.cursor() as cursor:
        cursor.execute(
         """SELECT * FROM MATA_KULIAH m WHERE m.kode NOT IN (SELECT id_mk FROM MK_MAHASISWA WHERE id_mahasiswa =%s) AND m.jumlah_mahasiswa > 0;
         """,[id_mahasiswa])
        rows = namedtuplefetchall(cursor)
        return rows 
        
def create_mata_kuliah(dict_detail):
    ''' menyimpan mata kuliah baru dalam pengumuman'''
    with connection.cursor() as cursor:
        try : 
            tuple_query = [
                dict_detail['mataKuliahKode'],
                dict_detail['mataKuliahPeriode'],
                dict_detail['mataKuliahNama'],
                dict_detail['mataKuliahMahasiswa'],
                dict_detail['mataKuliahKredit'],
                dict_detail['mataKuliahKurikulum'],
                dict_detail['mataKuliahTugasAkhir']
            ]
            cursor.execute("""insert into SIMKUSPA.mata_kuliah(kode,periode,nama,jumlah_mahasiswa,kredit,kurikulum,tugas_akhir) 
                                values(%s,%s,%s,%s,%s,%s,%s)""",tuple_query)
            return True
        except Exception :
            return False

def create_prasyarat(dict_detail_prasyarat):
    ''' menyimpan prasyarat baru dalam pengumuman'''
    with connection.cursor() as cursor:
        tuple_query = [
            dict_detail_prasyarat['mataKuliahKode'],
            dict_detail_prasyarat['prasyaratNama']
        ]
        cursor.execute("""insert into SIMKUSPA.prasyarat(id_mk,nama) 
                            values(%s,%s)""",tuple_query)
        return True

def get_all_dosen_pembimbing_mk(mk_id, mahasiswa_id):
    with connection.cursor() as cursor:
        tuple_query = [mk_id, mahasiswa_id]
        cursor.execute("""select mkd.status, d.nama from mk_mahasiswa_dosen as mkd 
                        join dosen as d on d.pengguna_id = mkd.id_dosen 
                        where mkd.id_mk=%s and mkd.id_mahasiswa=%s""", tuple_query)

        rows = namedtuplefetchall(cursor)
        return rows

def get_list_form_mk(id_mk):
    '''Mengembalikan list form untuk suatu mata kuliah'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT jenis, jenis_persyaratan from MK_FORM WHERE id_mk=%s;",[id_mk])
        list_form = namedtuplefetchall(cursor)
        return list_form

def create_form_mk(id_mk, jenis_form, jenis_persyaratan):
    '''Membuat form baru untuk suatu mata kuliah'''
    with connection.cursor() as cursor:
        try:
            cursor.execute("INSERT INTO MK_FORM(id_mk,jenis, jenis_persyaratan) VALUES(%s,%s,%s);",[id_mk, jenis_form, jenis_persyaratan])
            return True
        except Exception :
            return False
        

def get_detail_form_mk(id_mk, jenis_form):
    '''Mengembalikan seluruh file untuk suatu mata kuliah'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT p.username, p.id, f.url, f.nama_file from MK_MAHASISWA_FILE f , MAHASISWA m, PENGGUNA p WHERE p.id = m.pengguna_id AND m.pengguna_id=f.id_mahasiswa AND f.id_mk=%s AND jenis_form=%s ",[id_mk,jenis_form])
        list_file = namedtuplefetchall(cursor)
        return list_file

def get_detail_mata_kuliah(id_mk):
    '''Mengembalikan detail dari sebuah mata kuliah'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from MATA_KULIAH WHERE kode=%s", [id_mk])
        detail_mk = namedtuplefetchall(cursor)
        return detail_mk
    

def insert_mk_mahasiswa(dict_detail) :
    ''' menyimpan id_mk dan id_mahasiswa kedalam table mk_mahasiswa'''
    with connection.cursor() as cursor:
        tuple_query = [
            dict_detail['id_mk'],
            dict_detail['id_mahasiswa'],
        ]
        cursor.execute("insert into SIMKUSPA.mk_mahasiswa(id_mk,id_mahasiswa) values(%s,%s) ON CONFLICT (id_mk, id_mahasiswa) DO NOTHING",tuple_query)
        return True

def insert_request_mk_mahasiswa_dosen(dict_detail):
    '''Menyimpan request dosen pembimbing baru oleh mahasiswa'''
    with connection.cursor() as cursor:
        cursor.execute(
            """
            INSERT INTO MK_MAHASISWA_DOSEN VALUES(%s,%s,%s,'Menunggu persetujuan dosen', %s, %s)
            ON CONFLICT (id_mk, id_mahasiswa, id_dosen) DO NOTHING
            """, (dict_detail['id_mk'], dict_detail['id_mahasiswa'], dict_detail['id_dosen'], dict_detail['motivasi'], dict_detail['pesan'])
        )
        if dict_detail['id_topik'] != "":
            cursor.execute(
            """
            INSERT INTO TOPIK_MAHASISWA(mahasiswa_id, judul, deskripsi, batas_waktu, id_topik_dosen) VALUES(%s,%s,%s,%s,%s) RETURNING id
            """, (dict_detail['id_mahasiswa'], dict_detail['judul'], dict_detail['deskripsi'], dict_detail['batas_waktu'],dict_detail['id_topik'])
        )
        else :
            cursor.execute(
                """
                INSERT INTO TOPIK_MAHASISWA(mahasiswa_id, judul, deskripsi, batas_waktu) VALUES(%s,%s,%s,%s) RETURNING id
                """, (dict_detail['id_mahasiswa'], dict_detail['judul'], dict_detail['deskripsi'], dict_detail['batas_waktu'])
            )
        id_topik = namedtuplefetchall(cursor)[0]
        cursor.execute(
            """
            UPDATE MK_MAHASISWA_DOSEN SET id_topik_mahasiswa = %s WHERE id_mk = %s AND id_mahasiswa= %s AND id_dosen = %s;
            """,(id_topik, dict_detail['id_mk'],dict_detail['id_mahasiswa'],dict_detail['id_dosen'])
        )
        cursor.execute(
            """
            UPDATE MK_MAHASISWA SET id_topik_mahasiswa = %s WHERE id_mk = %s AND id_mahasiswa= %s;
            """,(id_topik, dict_detail['id_mk'],dict_detail['id_mahasiswa'])
        )
        return True

def create_form_persyaratan(dict_detail):
    '''Memasukkan form persyaratan ke database'''
    with connection.cursor() as cursor :
        tuple_query = [
            dict_detail['id_mk'],
            dict_detail['id_mahasiswa'],
            dict_detail['url'],
            dict_detail['nama_file'],
            dict_detail['jenis_form'],
        ]
        cursor.execute(
            """
            INSERT INTO SIMKUSPA.MK_MAHASISWA_FILE(id_mk, id_mahasiswa, url, nama_file, jenis_form) VALUES(%s,%s,%s,%s,%s)
            """,tuple_query)
        return True

def delete_form_persyaratan(id_mk,id_mahasiswa,jenis_form):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM SIMKUSPA.MK_MAHASISWA_FILE WHERE id_mk=%s AND id_mahasiswa=%s AND jenis_form=%s",[id_mk,id_mahasiswa,jenis_form])
        return True

def read_form_persyaratan(id_mk,id_mahasiswa,jenis_form):
    with connection.cursor() as cursor:
        cursor.execute("SELECT url,nama_file from SIMKUSPA.MK_MAHASISWA_FILE where id_mk=%s and id_mahasiswa=%s and jenis_form=%s",[id_mk,id_mahasiswa,jenis_form])
        detail_file = namedtuplefetchall(cursor)
        return detail_file

def create_request_migrasi(dict_detail):
    '''Memasukkan form persyaratan ke database'''
    with connection.cursor() as cursor :
        tuple_query = [
            dict_detail['id_mk'],
            dict_detail['id_mahasiswa'],
            dict_detail['periode_lama'],
            dict_detail['periode_baru'],
            dict_detail['status'],
        ]
        cursor.execute(
            """
            INSERT INTO SIMKUSPA.MK_MIGRASI(id_mk, id_mahasiswa, periode_lama, periode_baru, status) VALUES(%s,%s,%s,%s,%s)
            """,tuple_query)
        return True

def read_all_request_migrasi(id_mk):
    with connection.cursor() as cursor:
        cursor.execute("SELECT id, id_mk, id_mahasiswa, periode_lama, periode_baru, status, nama, npm from mk_migrasi inner join mahasiswa on mk_migrasi.id_mahasiswa=mahasiswa.pengguna_id where id_mk=%s and status='REQ'",[id_mk])
        detail_request_migrasi = namedtuplefetchall(cursor)
        return detail_request_migrasi

def read_detail_migrasi(id_mk,id_mahasiswa):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from SIMKUSPA.MK_MIGRASI where id_mk=%s and id_mahasiswa=%s",[id_mk,id_mahasiswa])
        detail_migrasi_per_mahasiswa = namedtuplefetchall(cursor)
        return detail_migrasi_per_mahasiswa

def accept_request_migrasi(dict_detail):
    id_mk = dict_detail['id_mk']
    id_mahasiswa = dict_detail['id_mahasiswa']
    new_id_mk = dict_detail['mk_tujuan']
    with connection.cursor() as cursor :
        cursor.execute("SELECT id_mk, id_mahasiswa, id_topik_mahasiswa, status from SIMKUSPA.MK_MAHASISWA where id_mk=%s and id_mahasiswa=%s",[id_mk,id_mahasiswa])
        detail_mk_mahasiswa = namedtuplefetchall(cursor)[0]
        new_detail_mk_mahasiswa = list(detail_mk_mahasiswa)
        new_detail_mk_mahasiswa[0]=new_id_mk
        cursor.execute(
            """
            INSERT INTO SIMKUSPA.MK_MAHASISWA(id_mk, id_mahasiswa, id_topik_mahasiswa, status) VALUES(%s,%s,%s,%s)
            """,new_detail_mk_mahasiswa)

        cursor.execute("SELECT * from SIMKUSPA.MK_MAHASISWA_FILE where id_mk=%s and id_mahasiswa=%s",[id_mk,id_mahasiswa])
        detail_mk_mahasiswa_file = namedtuplefetchall(cursor)
        if detail_mk_mahasiswa_file != []:
            for detail in detail_mk_mahasiswa_file:
                new_detail_mk_mahasiswa_file=list(detail)
                new_detail_mk_mahasiswa_file[0]=new_id_mk  
                cursor.execute(
                """
                INSERT INTO SIMKUSPA.MK_MAHASISWA_FILE(id_mk, id_mahasiswa, url, nama_file, jenis_form) VALUES(%s,%s,%s,%s,%s)
                """,new_detail_mk_mahasiswa_file)

        cursor.execute("SELECT * from SIMKUSPA.MK_MAHASISWA_DOSEN where id_mk=%s and id_mahasiswa=%s",[id_mk,id_mahasiswa])
        detail_mk_mahasiswa_dosen = namedtuplefetchall(cursor)
        for detail in detail_mk_mahasiswa_dosen:
            new_detail_mk_mahasiswa_dosen=list(detail)
            new_detail_mk_mahasiswa_dosen[0]=new_id_mk
            cursor.execute(
                """
                INSERT INTO SIMKUSPA.MK_MAHASISWA_DOSEN(id_mk, id_mahasiswa, id_dosen, status, motivasi, pesan, id_topik_mahasiswa) VALUES(%s,%s,%s,%s,%s,%s,%s)
                """,new_detail_mk_mahasiswa_dosen)

        cursor.execute(
            """
            UPDATE MK_MIGRASI SET status = 'Disetujui' WHERE id_mk = %s AND id_mahasiswa= %s;
            """,(id_mk,id_mahasiswa)
        )

        return True

def reject_request_migrasi(dict_detail):
    id_mk = dict_detail['id_mk']
    id_mahasiswa = dict_detail['id_mahasiswa']
    with connection.cursor() as cursor :
        cursor.execute(
            """
            UPDATE MK_MIGRASI SET status = 'Ditolak' WHERE id_mk = %s AND id_mahasiswa= %s;
            """,(id_mk,id_mahasiswa)
        )
        return True

def read_term_aktif():
    with connection.cursor() as cursor :
        cursor.execute(
            """
            SELECT * FROM TERM_AKTIF;
            """
        )
        term_aktif = namedtuplefetchall(cursor)
        return term_aktif
    