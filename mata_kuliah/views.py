
from multiprocessing import context
from django.shortcuts import render
from django.http import HttpResponse
from django.db import IntegrityError, connection
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse
from .db import create_mata_kuliah, create_prasyarat, create_request_migrasi,  get_all_dosen_pembimbing_mk, get_mk_all, get_mk_can_be_requested,get_mk_tersedia,\
         get_list_form_mk, create_form_mk, get_detail_form_mk, get_mk_tersedia_mahasiswa, insert_mk_mahasiswa, \
         get_detail_mata_kuliah, insert_request_mk_mahasiswa_dosen, create_form_persyaratan, delete_form_persyaratan,read_form_persyaratan,\
         create_request_migrasi, accept_request_migrasi, read_all_request_migrasi, read_term_aktif, reject_request_migrasi, read_detail_migrasi

from bimbingan.mail import MailRequest
from profiles.db import get_profile_mahasiswa, get_profile_dosen, get_profile_mahasiswa_for_email, \
                        get_profile_dosen_for_email

import json
import datetime

day_indo = {
    "Monday"    : "Senin",
    "Tuesday"   : "Selasa",
    "Wednesday" : "Rabu",
    "Thursday"  : "Kamis",
    "Friday"    : "Jumat",
    "Saturday"  : "Sabtu",
    "Sunday"    : "Minggu"
}

month_indo = {
    "January"   : "Januari",
    "February"  : "Februari",
    "March"     : "Maret",
    "April"     : "April",
    "May"       : "Mei",
    "June"      : "Juni",
    "July"      : "Juli",
    "August"    : "Agustus",
    "September" : "September",
    "October"   : "Oktober",
    "November"  : "November",
    "December"  : "Desember"
}

# Create your views here.
@require_GET
def mata_kuliah_pages(request, id_mahasiswa):
    context = {
        'mk_list': get_mk_all(id_mahasiswa)
    }

    return JsonResponse(context)

@require_GET
def mata_kuliah_can_be_requested(request, id_mahasiswa):
    context = {
        'mk_list': get_mk_can_be_requested(id_mahasiswa)
    }

    return JsonResponse(context)

@require_GET
def daftar_mk_pages(request):
    context = {
        'mk_tersedia' : get_mk_tersedia()
    }

    return JsonResponse(context)

@require_GET
def daftar_mk_pages_mahasiswa(request,id_mahasiswa):
    context = {
        'mk_tersedia' : get_mk_tersedia_mahasiswa(id_mahasiswa)
    }

    return JsonResponse(context)

@require_POST
def tambah_mata_kuliah(request):
    data = json.loads(request.body)
    kode= data["kode"]
    periode = data["periode"]
    nama = data["nama"]
    jumlah_mahasiswa = data["jumlah_mahasiswa"]
    kredit = data["kredit"]
    kurikulum = data["kurikulum"]
    prasyarat = data["prasyarat"]
    tugas_akhir = data["tugas_akhir"]

    dict_detail = {
         'mataKuliahKode' : kode+"_"+periode,
         'mataKuliahPeriode' : periode,
         'mataKuliahNama' : nama,
         'mataKuliahMahasiswa' : jumlah_mahasiswa,
         'mataKuliahKredit' : kredit,
         'mataKuliahKurikulum' : kurikulum,
         'mataKuliahTugasAkhir' : tugas_akhir,
        }

    dict_detail_prasyarat = {
        'mataKuliahKode' : kode+"_"+periode,
        'prasyaratNama' : prasyarat
    }

    if (create_mata_kuliah(dict_detail)) :
        
        if prasyarat!="":
            create_prasyarat(dict_detail_prasyarat)
        
        return JsonResponse({'status':'SUCCESS'})
    
    else : 
        return JsonResponse({'status':'FAILED'})

@require_GET
def dosen_pembimbing_mk(request, mk_id, mahasiswa_id):
    context = {
        'dospem_list': get_all_dosen_pembimbing_mk(mk_id, mahasiswa_id)
    }
    return JsonResponse(context)

@require_GET
def form_persyaratan(request,id_mk):
     list_form_persyaratan = get_list_form_mk(id_mk)
     return JsonResponse({'list_form_persyaratan': list_form_persyaratan})

@require_GET
def detail_form_persyaratan(request,id_mk,jenis_form):
     detail_form_persyaratan = get_detail_form_mk(id_mk,jenis_form)
     return JsonResponse({'detail_form_persyaratan': detail_form_persyaratan})

@require_POST
def tambah_form_persyaratan(request,id_mk):
    body = request.body.decode('utf-8')
    data = json.loads(body)
    nama_form = data['jenis_form']
    jenis_persyaratan_form = data['jenis_persyaratan']
    if create_form_mk(id_mk,nama_form, jenis_persyaratan_form) :
        return JsonResponse({'Success':'Success'})
    else :
        return JsonResponse({'Success':'Failed'})

@require_GET
def get_detail_mk(request,id_mk):
    detail_mk = get_detail_mata_kuliah(id_mk)[0]
    return JsonResponse({'detail_mk':detail_mk})

@require_POST
def mendaftarkan_mk_mahasiswa(request):
    data = json.loads(request.body)
    id_mk = data['id_mk']
    id_mahasiswa = data['id_mahasiswa']

    status = 'SUCCESS'

    dict_detail = {
            'id_mk': id_mk,
            'id_mahasiswa' : id_mahasiswa,
    }

    insert_mk_mahasiswa(dict_detail)

    context = {
         'status' : status
    }
    return JsonResponse(context)

@require_POST
def handle_request_dospem(request):
    data = json.loads(request.body)
    id_mk = data['id_mk']
    id_dosen = data['id_dosen']
    id_mahasiswa = data['id_mahasiswa']
    judul = data['judul']
    deskripsi = data['deskripsi']
    motivasi = data['motivasi']
    batas_waktu = data['batas_waktu']
    pesan = data['pesan']
    id_topik = data['id_topik']

    dict_detail = {
        'id_mk' : id_mk,
        'id_dosen' : id_dosen,
        'id_mahasiswa' : id_mahasiswa,
        'judul' : judul,
        'deskripsi' : deskripsi,
        'motivasi' : motivasi,
        'batas_waktu' : batas_waktu,
        'pesan' : pesan,
        'id_topik':id_topik
    }

    insert_request_mk_mahasiswa_dosen(dict_detail)
    __handle_send_mail(dict_detail)
    
    context = {
        'status' : 'SUCCESS'
    }
    return JsonResponse(context)

def __handle_send_mail(dict_detail):
    detail_mk = get_detail_mata_kuliah(dict_detail['id_mk'])[0]
    profile_mahasiswa = get_profile_mahasiswa_for_email(dict_detail['id_mahasiswa'])[0]
    profile_dosen = get_profile_dosen_for_email(dict_detail['id_dosen'])[0]

    batas_waktu_formatted = __waktu_format(dict_detail['batas_waktu'])
    today = datetime.date.today().strftime("%Y-%m-%d")
    tanggal_formatted = __waktu_format(today)

    content_mail = {
        'nama_mk': detail_mk[2],
        'nama_dosen': profile_dosen[0],
        'nama_mahasiswa': profile_mahasiswa[0],
        'npm': profile_mahasiswa[3],
        'prodi_mahasiswa': profile_mahasiswa[2],
        'judul_topik': dict_detail['judul'],
        'deskripsi_topik': dict_detail['deskripsi'],
        'batas_waktu': batas_waktu_formatted,
        'motivasi': dict_detail['motivasi'],
        'pesan': dict_detail['pesan'],
        'tanggal': tanggal_formatted,
    }
    mail = MailRequest(content_mail, 'email_permohonan_template.txt')

    email_mahasiswa = profile_mahasiswa[1]
    email_dosen = profile_dosen[1]

    creds = mail.authorize()
    mail.send_mail(creds, email_dosen, email_mahasiswa)

def __waktu_format(arr):
    waktu = [int(i) for i in arr.split('-')]
    waktu_obj = datetime.date(waktu[0], waktu[1], waktu[2])

    return f"{day_indo[waktu_obj.strftime('%A')]}, {waktu[2]} {month_indo[waktu_obj.strftime('%B')]} {waktu[0]}"

@require_POST
def upload_form_persyaratan(request):
    data = json.loads(request.body)
    id_mk= data["id_mk"]
    id_mahasiswa = data["id_mahasiswa"]
    url = data["url"]
    nama_file = data["nama_file"]
    jenis_form = data["jenis_form"]

    dict_detail = {
         'id_mk' : id_mk,
         'id_mahasiswa' : id_mahasiswa,
         'url' : url,
         'nama_file' : nama_file,
         'jenis_form' : jenis_form
        }

    create_form_persyaratan(dict_detail)

    return JsonResponse({'status':'SUCCESS'})

@require_GET
def hapus_form_persyaratan(request,id_mk,id_mahasiswa,jenis_form):   
    
    delete_form_persyaratan(id_mk,id_mahasiswa,jenis_form)

    status = 'SUCCESS'

    context = {
        'status' : status
    }

    return JsonResponse(context)

@require_GET
def file_form_persyaratan(request,id_mk,id_mahasiswa,jenis_form):  
    context = {
        'detail_file' : read_form_persyaratan(id_mk,id_mahasiswa,jenis_form)
        
    }

    return JsonResponse(context)

def get_periode_baru(periode_lama):
    periode_lama_array = periode_lama.split("-")
    tahun_periode = int(periode_lama_array[0])
    semester_periode = int(periode_lama_array[1])
    
    if semester_periode == 2 :
        tahun_periode_baru = tahun_periode + 1
        semester_periode_baru = 1
    else : 
        tahun_periode_baru = tahun_periode
        semester_periode_baru = int(semester_periode) + 1
    
    periode_baru = str(tahun_periode_baru) + "-" + str(semester_periode_baru)
    return periode_baru

@require_POST
def request_migrasi(request):
    data = json.loads(request.body)
    id_mk= data["id_mk"]
    id_mahasiswa = data["id_mahasiswa"]
    periode_lama = data["periode_lama"]
    periode_baru = get_periode_baru(periode_lama)
    status = "REQ"

    dict_detail = {
         'id_mk' : id_mk,
         'id_mahasiswa' : id_mahasiswa,
         'periode_lama' : periode_lama,
         'periode_baru' : periode_baru,
         'status' : status
        }

    create_request_migrasi(dict_detail)
    return JsonResponse({'status':'SUCCESS'})

@require_POST
def accept_migrasi(request):
    data = json.loads(request.body)
    id_mk= data["id_mk"]
    id_mahasiswa = data["id_mahasiswa"]

    periode_lama = id_mk.split('_')[1]
    periode_baru = get_periode_baru(periode_lama)
    mk_tujuan = id_mk.split('_')[0]+'_'+periode_baru
    
    migrasi_detail = {
         'id_mk' : id_mk,
         'id_mahasiswa' : id_mahasiswa,
         'mk_tujuan':mk_tujuan
        }

    accept_request_migrasi(migrasi_detail)

    return JsonResponse({'status':'SUCCESS'})

@require_GET
def get_all_request_migrasi(request,id_mk):  
    context = {
        'detail_migrasi' : read_all_request_migrasi(id_mk)
    }

    return JsonResponse(context)

@require_GET
def get_detail_migrasi(request,id_mk,id_mahasiswa):  
    context = {
        'detail_migrasi_per_mahasiswa' : read_detail_migrasi(id_mk,id_mahasiswa)
    }

    return JsonResponse(context)

@require_POST
def reject_migrasi(request):
    data = json.loads(request.body)
    id_mk= data["id_mk"]
    id_mahasiswa = data["id_mahasiswa"]
    migrasi_detail = {
         'id_mk' : id_mk,
         'id_mahasiswa' : id_mahasiswa
        }

    reject_request_migrasi(migrasi_detail)

    return JsonResponse({'status':'SUCCESS'})


@require_GET
def get_term_aktif(request):
    context = {
        'term_aktif' : read_term_aktif()
    }
    return JsonResponse(context)