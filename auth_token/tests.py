from django.test import TestCase
from django.test import SimpleTestCase
import json

class TokenTests(SimpleTestCase):
    
    def test_auth_token_running_successfully(self):
        response = self.client.get('/auth/token')
        self.assertEqual(response.status_code, 200)

    def test_auth_token_return_csrf(self):
        response = self.client.get('/auth/token')
        response_data = response.json()
        token = response_data['csrftoken']
        
        self.assertIsNotNone(token)