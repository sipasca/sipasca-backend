from django.urls import path
from . import views

app_name = 'auth_token'

urlpatterns = [
    path('token', views.get_csrf_token),
]