from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_status_topik_mahasiswa(status_id, dosen_id, mk_id):
    '''Mengembalikan daftar seluruh mahasiswa dengan status sesuai request'''
    with connection.cursor() as cursor:
        cursor.execute("""SELECT M.nama , MD.id_mahasiswa, TM.judul, MD.status, M.npm
        FROM simkuspa.MK_MAHASISWA_DOSEN MD, simkuspa.MAHASISWA M, simkuspa.TOPIK_MAHASISWA TM
        WHERE MD.id_dosen = %s AND MD.id_mk = %s AND MD.status = %s 
        AND M.pengguna_id = MD.id_mahasiswa AND TM.id = MD.id_topik_mahasiswa;""", 
        [dosen_id, mk_id, status_id])
        daftar_status = namedtuplefetchall(cursor)
        return daftar_status

def get_all_mahasiswa_mk(mk_id):
    '''Mengembalikan daftar seluruh status mahasiswa pada suatu mata kuliah'''
    with connection.cursor() as cursor:
        cursor.execute("""SELECT MK.id_mahasiswa, M.nama, M.npm,d.nama_gelar as nama_dospem, MK.status, MK.last_modified \
        FROM MK_MAHASISWA MK LEFT OUTER JOIN MK_MAHASISWA_DOSEN MKD \
        on MK.id_mk=MKD.id_MK AND MK.id_mahasiswa = MKD.id_mahasiswa \
        LEFT OUTER JOIN MAHASISWA M on MK.id_mahasiswa = M.pengguna_id \
        LEFT OUTER JOIN DOSEN d on MKD.id_dosen = D.pengguna_id \
        WHERE MK.id_mk = %s ;""", [mk_id])
        daftar_mahasiswa_mk = namedtuplefetchall(cursor)
        return daftar_mahasiswa_mk

def update_status_masa_bimbingan_mahasiswa(mk_id, mahasiswa_id):
    with connection.cursor() as cursor:
        cursor.execute("""UPDATE simkuspa.MK_MAHASISWA_DOSEN 
        set status = 'Masa bimbingan' WHERE id_mk=%s AND 
        id_mahasiswa=%s;""",[mk_id, mahasiswa_id])

def accept_status_mahasiswa_bimbingan(id_mk,id_mahasiswa,id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("""UPDATE simkuspa.MK_MAHASISWA_DOSEN set status = 'Disetujui dosen pembimbing' WHERE id_mk=%s AND id_mahasiswa=%s AND id_dosen=%s""",[id_mk, id_mahasiswa,id_dosen])
        cursor.execute("""SELECT t.id_topik_dosen FROM simkuspa.TOPIK_MAHASISWA t, simkuspa.MK_MAHASISWA_DOSEN m WHERE t.id= m.id_topik_mahasiswa AND m.id_mk=%s AND m.id_mahasiswa=%s AND id_dosen=%s""",[id_mk, id_mahasiswa,id_dosen])
        id_topik_dosen = namedtuplefetchall(cursor)[0]
        cursor.execute("""UPDATE simkuspa.TOPIK_DOSEN SET jml_diterima = jml_diterima + 1 WHERE id=%s""",[id_topik_dosen])
        is_topik_closed = check_topik_to_close_or_not(id_topik_dosen)
        result_arr = []
        if is_topik_closed :
            cursor.execute("""SELECT m.id_mk, m.id_mahasiswa, m.id_dosen FROM MK_MAHASISWA_DOSEN m, TOPIK_MAHASISWA t WHERE m.id_topik_mahasiswa= t.id AND t.id_topik_dosen =%s AND m.status ='Menunggu persetujuan dosen' """,[id_topik_dosen])
            result_arr = namedtuplefetchall(cursor)
        return result_arr

def check_topik_to_close_or_not(id_topik):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT batas_diterima, jml_diterima FROM TOPIK_DOSEN t WHERE id =%s """,[id_topik])
        result = namedtuplefetchall(cursor)[0]
        batas_diterima = result.batas_diterima 
        jml_diterima = result.jml_diterima
        if batas_diterima == jml_diterima:
            closed = True
        else :
            closed = False
        return closed

def reject_status_mahasiswa_bimbingan(id_mk,id_mahasiswa,id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("""DELETE FROM simkuspa.MK_MAHASISWA_DOSEN WHERE id_mk=%s AND id_mahasiswa=%s AND id_dosen=%s""",[id_mk, id_mahasiswa,id_dosen])
        return True

def get_detail_mahasiswa_mk(mk_id, mahasiswa_id):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT MK.id_mahasiswa, M.nama, M.npm, MK.status
        FROM simkuspa.MK_MAHASISWA MK, simkuspa.MAHASISWA M
        WHERE MK.id_mk = %s AND MK.id_mahasiswa = %s 
        AND M.pengguna_id = MK.id_mahasiswa;""", [mk_id, mahasiswa_id])
        result = namedtuplefetchall(cursor)
        cursor.execute("""SELECT url, nama_file, jenis_form 
        FROM simkuspa.MK_MAHASISWA_FILE WHERE id_mk = %s 
        AND id_mahasiswa = %s ;""", [mk_id, mahasiswa_id])
        result.append(namedtuplefetchall(cursor))
        cursor.execute("""SELECT D.nama AS dosen
        FROM simkuspa.MK_MAHASISWA_DOSEN MK, simkuspa.DOSEN D
        WHERE MK.id_mk = %s AND MK.id_mahasiswa = %s 
        AND D.pengguna_id = MK.id_dosen;""", [mk_id, mahasiswa_id])
        result.append(namedtuplefetchall(cursor))
        return result
