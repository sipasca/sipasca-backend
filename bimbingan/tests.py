from django.test import TestCase
import json

from unittest import mock

from bimbingan.views import accept_mahasiswa_bimbingan
from .mail import MailRequest
from mata_kuliah.db import insert_request_mk_mahasiswa_dosen

send_type = "application/json"
content_data = {
    'nama_mk': 'Tesis',
    'panggilan': 'Bapak',
    'nama_dosen': 'A',
    'nama_mahasiswa': 'Chaehun Choi',
    'npm': '2206000000',
    'prodi_mahasiswa': 'Magister Ilmu Komputer',
    'judul_topik': 'Implementasi Convolutional Neural Network dalam Identifikasi Tulisan Hangul',
    'deskripsi_topik': """Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at risus nec ipsum hendrerit pretium. Donec dapibus magna quis efficitur pretium. Maecenas luctus varius fermentum. In eros dui, ultricies ut lorem at, dignissim pharetra risus. Nulla a turpis eu sem bibendum molestie. Aenean auctor ante eros, et posuere metus pellentesque sed. Praesent id mattis erat. Fusce imperdiet consequat justo ut pellentesque. Suspendisse pulvinar pharetra nisi, ac tempor quam. Proin id bibendum ipsum. Nullam mattis libero vel nibh ultrices, ut sodales nunc lacinia. Nullam id vehicula nisl. Ut ultrices metus eu ullamcorper facilisis. Quisque vel dignissim nunc. Curabitur ullamcorper tincidunt magna quis aliquam.<br /> <br />Vivamus viverra mi ac nisl pharetra maximus. Duis cursus, dui vehicula ultrices gravida, sem diam viverra augue, ut gravida eros enim eget neque. Donec cursus lacinia justo, quis efficitur magna. Suspendisse vestibulum lacus ac euismod sodales. Morbi felis tortor, sodales eu justo vitae, auctor maximus nulla. Pellentesque pharetra diam nec turpis ullamcorper finibus. Vivamus non mi consequat, dignissim sem at, facilisis sem. Donec lobortis justo et risus finibus rhoncus. Sed vehicula id enim ullamcorper congue. Nunc vitae quam id libero dapibus varius. Nullam ante nunc, molestie vitae quam non, fringilla condimentum nisl. Mauris accumsan tempus tortor, at vehicula nulla bibendum eget.""",
    'batas_waktu': '31 Desember 2022',
    'motivasi': 'Saya mengambil topik ini karena saya mempelajari tulisan Hangul saat saya menempuh pendidikan di Korea.',
    'pesan': 'Saya melihat publikasi Bapak mengenai Implementasi Convolutional Neural Network untuk tulisan Kanji sehingga saya merasa penelitian yang saya lakukan akan sangat terbantu oleh keahlian dan pengalaman Bapak.',
    'tanggal': 'Kamis, 7 April 2022',
}
template = 'email_permohonan_template.txt'
reject_mahasiswa_bimbingan_url = '/rejectMahasiswaBimbingan/'
accept_mahasiswa_bimbingan_url = '/acceptMahasiswaBimbingan/'

# Create your tests here.
class BimbinganTest(TestCase):
    def test_daftar_status_topik_mahasiswa_success(self):
        response = self.client.get('/daftar/Menunggu persetujuan dosen/D1234567890/CSGE802097/')
        self.assertEqual(response.status_code, 200)
    
    @mock.patch('googleapiclient.discovery.Resource')
    def test_accept_mahasiswa_bimbingan_successfully(self, mock_build):
        dummy_request_1 = {
        'id_mk' : 'CSGE802097',
        'id_dosen' : 'D1234567890',
        'id_mahasiswa' : 'M1900000000',
        'judul' : 'judul',
        'deskripsi' : 'deskripsi',
        'motivasi' : 'motivasi',
        'batas_waktu' : '2022-3-3',
        'pesan' : 'pesan',
        'id_topik':'28'
         }
        dummy_request_2 = {
        'id_mk' : 'CSGE802097',
        'id_dosen' : 'D1234567890',
        'id_mahasiswa' : 'M1900000001',
        'judul' : 'judul',
        'deskripsi' : 'deskripsi',
        'motivasi' : 'motivasi',
        'batas_waktu' : '2022-3-3',
        'pesan' : 'pesan',
        'id_topik':'28'
        }
        insert_request_mk_mahasiswa_dosen(dummy_request_1)
        insert_request_mk_mahasiswa_dosen(dummy_request_2)
        data = {
         'id_mk' : 'CSGE802097',
         'id_mahasiswa' : 'M1900000001',
         'id_dosen' : 'D1234567890',
         }
        response = self.client.post(accept_mahasiswa_bimbingan_url, json.dumps(data), content_type=send_type)
        self.assertEqual(response.status_code,200)

    @mock.patch('googleapiclient.discovery.Resource')
    def test_send_mail_accept(self, mock_build):
        dummy_request_1 = {
        'id_mk' : 'CSGE802097',
        'id_dosen' : 'D1234567890',
        'id_mahasiswa' : 'M1900000000',
        'judul' : 'judul',
        'deskripsi' : 'deskripsi',
        'motivasi' : 'motivasi',
        'batas_waktu' : '2022-3-3',
        'pesan' : 'pesan',
        'id_topik':'28'
         }
        dummy_request_2 = {
        'id_mk' : 'CSGE802097',
        'id_dosen' : 'D1234567890',
        'id_mahasiswa' : 'M1900000001',
        'judul' : 'judul',
        'deskripsi' : 'deskripsi',
        'motivasi' : 'motivasi',
        'batas_waktu' : '2022-3-3',
        'pesan' : 'pesan',
        'id_topik':'28'
        }
        insert_request_mk_mahasiswa_dosen(dummy_request_1)
        insert_request_mk_mahasiswa_dosen(dummy_request_2)
        data = {
         'id_mk' : 'CSGE802097',
         'id_mahasiswa' : 'M1900000001',
         'id_dosen' : 'D1234567890',
         }
        response = self.client.post(accept_mahasiswa_bimbingan_url, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)
    
    @mock.patch('googleapiclient.discovery.Resource')
    def test_reject_mahasiswa_bimbingan_successfully(self, mock_build):
        data = {
         'id_mk' : 'CSGE802097',
         'id_mahasiswa' : 'M1900000001',
         'id_dosen' : 'D1234567890',
         }
        response = self.client.post(reject_mahasiswa_bimbingan_url, json.dumps(data), content_type=send_type)
        self.assertEqual(response.status_code,200)
    
    @mock.patch('googleapiclient.discovery.Resource')
    def test_send_mail_rejection(self, mock_build):
        data = {
         'id_mk' : 'CSGE802097',
         'id_mahasiswa' : 'M1900000001',
         'id_dosen' : 'D1234567890',
         }
        response = self.client.post(reject_mahasiswa_bimbingan_url, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)
        
    def test_mail_created_correctly(self):
        mail = MailRequest(content_data, template)
        self.assertEqual(mail.template_subject, "Permohonan Bimbingan Mata Kuliah Tesis")
        self.assertIn("<b>Judul Topik:</b> Implementasi Convolutional Neural Network dalam Identifikasi Tulisan Hangul", mail.template_body)
    
    def test_mail_authorization_successfully(self):
        mail = MailRequest(content_data, template)
        creds = mail.authorize()
        self.assertIsNotNone(creds)
    
    @mock.patch('googleapiclient.discovery.Resource')
    def test_mail_sent_successfully(self, mock_resource):
        mail = MailRequest(content_data, template)
        creds = mail.authorize()

        to = 'kangenzppl@gmail.com'
        reply_to = 'kangenzppl@gmail.com'

        res = mail.send_mail(creds, to, reply_to)
        self.assertIsNotNone(res)
    
    def test_list_all_mahasiswa_mk_success(self):
        response = self.client.get('/daftarAllMahasiswaMK/CSGE802097/')
        self.assertEqual(response.status_code, 200,)

    def test_update_status_to_masa_bimbingan_successfully(self):
        response = self.client.get('/updateStatusToMasaBimbingan/CSGE801092/M1900000000/')
        self.assertEqual(response.status_code,200)
    
    def test_read_detail_mahasiswa_dalam_suatu_mk_successfully(self):
        response = self.client.get('/readDetailMahasiswaMK/CSGE801092/M1900000000/')
        self.assertEqual(response.status_code,200)
