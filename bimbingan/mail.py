from django.templatetags.static import static
import os
import environ
import json

from string import Template

from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import base64

class MailRequest():
    def __init__(self, data, template_name):
        self.sender = os.environ.get('MAIL_SENDER')
        self.template_subject = "Permohonan Bimbingan Mata Kuliah " + data['nama_mk']

        template = os.path.join(os.path.dirname(__file__), 'static/' + template_name)
        with open(template, 'r') as f:
            email_template = Template(f.read())
            self.template_body = email_template.substitute(data)
    
    def authorize(self):
        """
        Authorization with credential info stored in environment variable
            Args: 
                None

            Returns:
                Google Auth Credential Object
        """
        SCOPES = ['https://www.googleapis.com/auth/gmail.readonly', 'https://www.googleapis.com/auth/gmail.send']

        PRODUCTION = os.environ.get('DATABASE_URL') != None
        if not PRODUCTION:
            env = environ.Env()
            env.read_env()

        creds_info = os.environ.get('MAIL_CREDS')
        creds_info = json.loads(creds_info)

        creds = Credentials.from_authorized_user_info(creds_info, SCOPES)
        return creds
    
    def send_mail(self, creds, to, reply_to):
        """
        Sending mail with reply_to set to an email address
            Args: 
                creds: Google Auth Credential Object
                to: Email address of the receiver.
                reply_to: Email address for reply-to.

            Returns:
                Sent Message
        """
        service = build('gmail', 'v1', credentials=creds)

        message = self.create_message(to, reply_to)
        result = self.send_message(service, message)
        return result
    
    def create_message(self, to, reply_to):
        """Create a message object for an email.

            Args:
                to: Email address of the receiver.
                reply_to: Email address for reply-to.

            Returns:
                An object containing a base64url encoded email object.
        """
        msg = MIMEMultipart()
        msg['to'] = to
        msg['from'] = self.sender
        msg['subject'] = self.template_subject
        msg['reply-to'] = reply_to

        body = self.template_body.replace('\n', '<br />')
        body = body.replace('\t', '&emsp;')

        content = MIMEText(self.template_body, 'html')
        msg.attach(content)

        raw = base64.urlsafe_b64encode(msg.as_bytes())
        raw = raw.decode()
        return {'raw': raw}

    def send_message(self, service, message):
        """Send an email message.

        Args:
            service: Authorized Gmail API service instance.
            message: Message to be sent.

        Returns:
            Sent Message.
        """
        message = service.users().messages().send(userId='me', body=message).execute()
        return message
