from django.shortcuts import render
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse
from bimbingan.db import get_status_topik_mahasiswa, update_status_masa_bimbingan_mahasiswa,\
    accept_status_mahasiswa_bimbingan,reject_status_mahasiswa_bimbingan, get_all_mahasiswa_mk, get_detail_mahasiswa_mk
import json
from bimbingan.mail import MailRequest
from mata_kuliah.db import get_detail_mata_kuliah
from profiles.db import get_profile_dosen_for_email, get_profile_mahasiswa_for_email

from bimbingan.mail import MailRequest
from mata_kuliah.db import get_detail_mata_kuliah
from profiles.db import get_profile_dosen_for_email, get_profile_mahasiswa_for_email

# Create your views here.
@require_GET
def daftar_status_topik_mahasiswa(request, status_id, dosen_id, mk_id):
    return JsonResponse({ 
        'list_status_topik_mahasiswa': get_status_topik_mahasiswa(status_id, dosen_id, mk_id)
    })


@require_GET
def update_status_to_masa_bimbingan(request, mk_id, mahasiswa_id):
    update_status_masa_bimbingan_mahasiswa(mk_id, mahasiswa_id)
    context = {
         'status' : 'SUCCESS'
    }
    return JsonResponse(context)

@require_GET
def daftar_all_mahasiswa_mk(request,mk_id):
    return JsonResponse({ 
        'list_all_mahasiswa_mk': get_all_mahasiswa_mk(mk_id)
    })

@require_POST
def accept_mahasiswa_bimbingan(request):
    data = json.loads(request.body)
    id_mk = data['id_mk']
    id_dosen = data['id_dosen']
    id_mahasiswa = data['id_mahasiswa']
    results = accept_status_mahasiswa_bimbingan(id_mk,id_mahasiswa,id_dosen)
    dict_detail={
        'id_mk' : id_mk, 
        'id_mahasiswa' : id_mahasiswa,
        'id_dosen' : id_dosen
    }
    __handle_send_email_response(dict_detail,"ACCEPT")
    if len(results) > 0 :
        for result in results :
            rejecting_bimbingan(result.id_mk,result.id_dosen,result.id_mahasiswa)
    context = {
         'status' : 'SUCCESS'
    }
    return JsonResponse(context)


@require_POST
def reject_mahasiswa_bimbingan(request):
    data = json.loads(request.body)
    id_mk = data['id_mk']
    id_dosen = data['id_dosen']
    id_mahasiswa = data['id_mahasiswa']
    rejecting_bimbingan(id_mk,id_dosen,id_mahasiswa)
    context = {
         'status' : 'SUCCESS'
    }
    return JsonResponse(context)

def rejecting_bimbingan(id_mk, id_dosen,id_mahasiswa):
    reject_status_mahasiswa_bimbingan(id_mk,id_mahasiswa,id_dosen)
    dict_detail={
        'id_mk' : id_mk, 
        'id_mahasiswa' : id_mahasiswa,
        'id_dosen' : id_dosen
    }
    __handle_send_email_response(dict_detail,"REJECT")
    return True

def __handle_send_email_response(dict_detail, response):
    detail_mk = get_detail_mata_kuliah(dict_detail['id_mk'])[0]
    profile_mahasiswa = get_profile_mahasiswa_for_email(dict_detail['id_mahasiswa'])[0]
    profile_dosen = get_profile_dosen_for_email(dict_detail['id_dosen'])[0]
    content_mail = {
        'nama_mk': detail_mk[2],
        'nama_dosen': profile_dosen[0],
        'nama_mahasiswa': profile_mahasiswa[0],
    }
    if response == "ACCEPT" :
        mail = MailRequest(content_mail, 'email_penerimaan_template.txt')
    else :
        mail = MailRequest(content_mail, 'email_penolakan_template.txt')
    email_mahasiswa = profile_mahasiswa[1]
    email_dosen = profile_dosen[1]
    creds = mail.authorize()
    mail.send_mail(creds, email_mahasiswa, email_dosen)


@require_GET
def read_detail_mahasiswa_mk(request, mk_id, mahasiswa_id):
    return JsonResponse({ 
        'detail_mahasiswa_mk': get_detail_mahasiswa_mk(mk_id, mahasiswa_id)
    })
