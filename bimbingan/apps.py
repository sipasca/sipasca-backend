from django.apps import AppConfig


class BimbinganConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bimbingan'
