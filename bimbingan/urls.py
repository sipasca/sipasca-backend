from unicodedata import name
from django.urls import path
from . import views

app_name = 'bimbingan'

urlpatterns = [
    path('daftar/<str:status_id>/<str:dosen_id>/<str:mk_id>/', views.daftar_status_topik_mahasiswa, name="daftarStatusTopikMahasiswa"),
    path('acceptMahasiswaBimbingan/',views.accept_mahasiswa_bimbingan, name="accept_mahasiswa_bimbingan"),
    path('rejectMahasiswaBimbingan/',views.reject_mahasiswa_bimbingan,name="reject_mahasiswa_bimbingan"),
    path('daftarAllMahasiswaMK/<str:mk_id>/',views.daftar_all_mahasiswa_mk,name="daftarAllMahasiswaMk"),
    path('updateStatusToMasaBimbingan/<str:mk_id>/<str:mahasiswa_id>/',views.update_status_to_masa_bimbingan,name="updateStatusToMasaBimbingan"),
    path('readDetailMahasiswaMK/<str:mk_id>/<str:mahasiswa_id>/',views.read_detail_mahasiswa_mk,name="readDetailMahasiswaMK")
]
