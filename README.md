# SIMKUSPA
## Sistem Informasi Mata Kuliah Pasca
SIMKUSPA is a web-based information system that can be used by Postgraduate (S2) students to help them when they take *Mata Kuliah Spesial*.

## Environment
SIMKUSPA backend runs with Django Framework. 

### a. Running
Run this application in virtual environment with:
    
    python manage.py runserver

### b. Testing
Please do not forget to include the `keepdb` tag to prevent Django from destroying the testing database after it run all the tests:

    python manage.py test --keepdb

Testing a spesific test file with:

    python manage.py test <file_name>.tests --keepdb

### c. Coverage
To check and ensuring that coverage covers all the newest development the app has, delete the cache report first:

    coverage erase

Then, run all the test with coverage:

    coverage run manage.py test --keepdb

Make a report and see your coverage:

    coverage xml -i
    coverage report -m
