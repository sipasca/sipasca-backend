from django.urls import path
from . import views

app_name = 'profiles'

urlpatterns = [
    path('getAllBidangKepakaran', views.get_list_bidang_kepakaran, name="get_all_bidang_kepakaran"),
    path('getDosenByBidangKepakaran/<str:id_bidang>', views.list_dosen_by_bidang_kepakaran, name="get_dosen_by_bidang_kepakaran"),
    path('getProfileMahasiswa/<str:id_mahasiswa>',views.detail_profile_mahasiswa,name="get_profile_mahasiswa"),
    path('updateProfileDosen', views.update_profile_dosen,name="update_profile_dosen"),
    path('getProfileDosen/<str:id_dosen>', views.detail_profile_dosen, name="get_profile_dosen"),
    path('updateCvMahasiswa/<str:id_mahasiswa>', views.update_cv_profile_mahasiswa, name="update_cv_profile_mahasiswa"),
    path('updateFotoMahasiswa/<str:id_mahasiswa>', views.update_foto_profile_mahasiswa, name="update_foto_profile_mahasiswa"),
    path('updateStatusAvailableDosen/<str:id_dosen>/<str:status>',views.updating_status_available_dosen,name="updating_status_available_dosen"),
    path('getAllDosen/', views.list_all_dosen, name="get_all_dosen"),
]