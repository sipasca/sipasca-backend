
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse

from .db import get_all_bidang_kepakaran, get_all_dosen_in_bidang_kepakaran, \
                get_profile_mahasiswa, get_profile_dosen, get_list_url_dosen, get_list_pendidikan_dosen, get_list_publikasi_dosen,\
                update_cv_mahasiswa, update_foto_mahasiswa, get_list_bidang_kepakaran_dosen, update_status_available_dosen, \
                updating_all_profile_dosen,get_all_dosen
import json

# Create your views here.
@require_GET
def get_list_bidang_kepakaran(request):
    context = {
        'list_bidang_kepakaran': get_all_bidang_kepakaran()
    }
    return JsonResponse(context)

@require_GET
def list_dosen_by_bidang_kepakaran(request, id_bidang):
    context = {
        'list_dosen': get_all_dosen_in_bidang_kepakaran(id_bidang)
    }

    return JsonResponse(context)

@require_GET
def detail_profile_mahasiswa(request, id_mahasiswa):
    context = {
        'profile_mahasiswa': get_profile_mahasiswa(id_mahasiswa)
    }
    return JsonResponse(context)

@require_GET
def detail_profile_dosen(request, id_dosen):
    context = {
        'profile_dosen': get_profile_dosen(id_dosen),
        'list_publikasi': get_list_publikasi_dosen(id_dosen),
        'list_url': get_list_url_dosen(id_dosen),
        'list_pendidikan' : get_list_pendidikan_dosen(id_dosen),
        'list_kepakaran' :get_list_bidang_kepakaran_dosen(id_dosen)
    }
    return JsonResponse(context)

@require_GET
def update_profile_dosen(request) :
    updating_all_profile_dosen()
    context= {
        'status':'SUCCESS'
    }
    return JsonResponse(context)


@require_POST
def update_cv_profile_mahasiswa(request, id_mahasiswa):
    data = json.loads(request.body)
    cv_url = data["cv_url"]
    cv_name = data["cv_name"]
    
    dict_detail = {
            'cv_url': cv_url,
            'cv_name': cv_name
    }

    update_cv_mahasiswa(id_mahasiswa, dict_detail)

    context = {
        'status': 'Success'
    }
    return JsonResponse(context)

@require_POST
def update_foto_profile_mahasiswa(request, id_mahasiswa):
    data = json.loads(request.body)
    image_url = data["image_url"]

    update_foto_mahasiswa(id_mahasiswa,image_url)

    context = {
        'status': 'Success'
    }
    return JsonResponse(context)

@require_GET
def updating_status_available_dosen(request, id_dosen, status):
    change_status_to_available = status == "1"
    if change_status_to_available :
        new_status = "Available"
    else :
        new_status = "Unavailable"
    update_status_available_dosen(id_dosen,new_status)
    context = {
        'status': 'Success'
    }
    return JsonResponse(context)
    
@require_GET
def list_all_dosen(request):
    context = {
        'list_all_dosen': get_all_dosen()
    }

    return JsonResponse(context)
