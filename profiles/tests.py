from django.test import TestCase
from unittest import mock
from .db import create_mahasiswa
from .static.test import html_content
import json

type = "application/json"
update_profile_dosen = '/updateProfileDosen'


def mocked_requests_get_special_case(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code
            self.content = bytes(html_content, 'utf-8')

    return MockResponse({"content": "value2"}, 200)

# Create your tests here.
class ProfileTest(TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get_special_case)
    def test_update_profile_dosen_special_case_successfully(self, mock_requests_get_special_case):
        response = self.client.get(update_profile_dosen)
        self.assertEqual(response.status_code, 200)

    @mock.patch('requests.get', side_effect=mocked_requests_get_special_case)
    def test_update_profile_dosen_special_case_return_success(self, mock_requests_get_special_case):
        response = self.client.get(update_profile_dosen)
        data = response.json()
        self.assertEqual(data['status'], 'SUCCESS')
    
    def test_get_all_bidang_kepakaran_success(self):
        response = self.client.get('/getAllBidangKepakaran')
        self.assertEqual(response.status_code, 200)
    
    def test_get_dosen_in_bidang_kepakaran_success(self):
        response = self.client.get('/getDosenByBidangKepakaran/SEG')
        self.assertEqual(response.status_code, 200)

    def test_get_profile_mahasiswa(self):
        create_mahasiswa("dummy","dummy","dummy","dummy")
        response = self.client.get('/getProfileMahasiswa/dummy_id')
        self.assertEqual(response.status_code,200)
    
    def test_get_profile_dosen(self):
        response = self.client.get('/getProfileDosen/D1906293045')
        self.assertEqual(response.status_code,200)

    def test_update_cv_mahasiswa(self):
        detail_cv = {
        'cv_url' : 'dummy.com',
        'cv_name': 'dummy_cv'
        }
        response = self.client.post('/updateCvMahasiswa/dummy_id',json.dumps(detail_cv), content_type = type)
        self.assertEqual(response.status_code, 200)

    def test_update_foto_mahasiswa(self):
        detail_foto = {
        'image_url' : 'dummy.com',
        }
        response = self.client.post('/updateFotoMahasiswa/dummy_id',json.dumps(detail_foto), content_type = type)
        self.assertEqual(response.status_code, 200)

    def test_update_status_dosen_to_available(self):
        response = self.client.get('/updateStatusAvailableDosen/dummy/1')
        self.assertEqual(response.status_code, 200)
    
    def test_update_status_dosen_to_unavailable(self):
        response = self.client.get('/updateStatusAvailableDosen/dummy/0')
        self.assertEqual(response.status_code, 200)

    def test_get_all_dosen(self):
        response = self.client.get('/getAllDosen/')
        self.assertEqual(response.status_code, 200)