
from django.db import connection
from collections import namedtuple

import requests as reqs
from bs4 import BeautifulSoup
import json

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_all_profile_url_dosen():
    with connection.cursor() as cursor:
        cursor.execute("""SELECT pengguna_id, profile_url FROM DOSEN""")
        rows = namedtuplefetchall(cursor)
        return rows

def get_all_bidang_kepakaran():
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM JENIS_KEPAKARAN")
        rows = namedtuplefetchall(cursor)
        return rows

def get_all_dosen_in_bidang_kepakaran(id_bidang):
    with connection.cursor() as cursor:
        cursor.execute("""SELECT BK.id_dosen, D.nama_gelar, D.email, D.profile_url FROM DOSEN D JOIN BIDANG_KEPAKARAN_DOSEN BK 
                        ON D.pengguna_id = BK.id_dosen WHERE BK.id_bidang_kepakaran=%s AND D.status='Available';""",[id_bidang])
        rows = namedtuplefetchall(cursor)
        return rows

def get_profile_mahasiswa(id_mahasiswa):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM MAHASISWA WHERE pengguna_id =%s ;",[id_mahasiswa])
        rows = namedtuplefetchall(cursor)
        return rows

def get_profile_mahasiswa_for_email(id_mahasiswa):
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama,email,prodi,npm FROM MAHASISWA WHERE pengguna_id =%s ;",[id_mahasiswa])
        rows = namedtuplefetchall(cursor)
        return rows

def get_profile_dosen(id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM DOSEN WHERE pengguna_id =%s ;",[id_dosen])
        rows = namedtuplefetchall(cursor)
        return rows

def get_profile_dosen_for_email(id_mahasiswa):
    with connection.cursor() as cursor:
        cursor.execute("SELECT nama,email FROM DOSEN WHERE pengguna_id =%s ;",[id_mahasiswa])
        rows = namedtuplefetchall(cursor)
        return rows

def create_mahasiswa(id,password,username,name):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO PENGGUNA VALUES(%s,%s,%s)ON CONFLICT (id) DO NOTHING ;",[id,password,username])
        cursor.execute("INSERT INTO MAHASISWA VALUES(%s,%s)ON CONFLICT (pengguna_id) DO NOTHING;",[id,name])

def get_list_pendidikan_dosen(id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT pendidikan FROM LIST_PENDIDIKAN_DOSEN WHERE id_dosen =%s ORDER BY(no) ;",[id_dosen])
        rows = namedtuplefetchall(cursor)
        return rows

def get_list_publikasi_dosen(id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT judul_publikasi,url_publikasi FROM LIST_PUBLIKASI_DOSEN WHERE id_dosen =%s ORDER BY(no) ;",[id_dosen])
        rows = namedtuplefetchall(cursor)
        return rows

def get_list_url_dosen(id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT jenis_url,url FROM LIST_URL_DOSEN WHERE id_dosen =%s ORDER BY(jenis_url) ;",[id_dosen])
        rows = namedtuplefetchall(cursor)
        return rows

def get_list_bidang_kepakaran_dosen(id_dosen):
    with connection.cursor() as cursor:
        cursor.execute("SELECT j.nama FROM BIDANG_KEPAKARAN_DOSEN d,JENIS_KEPAKARAN j WHERE j.id_bidang_kepakaran = d.id_bidang_kepakaran AND d.id_dosen =%s ;",[id_dosen])
        rows = namedtuplefetchall(cursor)
        return rows

def updating_all_profile_dosen():
    list_profile_dosen = get_all_profile_url_dosen()
    for profile in list_profile_dosen:
        id_dosen = profile[0]
        url = profile[1]
        updating_profile_dosen(id_dosen,url)
    return True

def crawl_profile_url(target_email):
    profile_url=""
    target_url = "https://cs.ui.ac.id/staf-pengajar-tetap/"
    page = reqs.get(target_url)
    current_search = BeautifulSoup(page.content,"html.parser")
    list_dosen = current_search.findAll(class_="gdlr-core-personnel-list-content-wrap")
    for dosen in list_dosen:
        search_email_html = dosen.find(class_="kingster-personnel-info-list kingster-type-email")
        if search_email_html is not None:
            search_email = search_email_html.text
            check_is_email_same = target_email == search_email
            if(check_is_email_same):
                profile_url_html = dosen.find(class_="gdlr-core-personnel-list-button gdlr-core-button")
                profile_url = profile_url_html.get("href")
    return profile_url


def updating_profile_dosen(id_dosen,url):
    url_profile_is_not_empty = url != ""
    if url_profile_is_not_empty : 
        print("?"+url)
        page = reqs.get(url)
        current_search = BeautifulSoup(page.content,"html.parser")
        updating_general_info_dosen(current_search,id_dosen)
        updating_list_pendidikan_and_publikasi_dosen(current_search,id_dosen)
        updating_list_url_dosen(current_search,id_dosen)
        updating_bidang_kepakaran_dosen(current_search,id_dosen)

def crawl_nama_dosen(current_search):
    html_nama_dosen = current_search.find(class_="gdlr-core-title-item-title gdlr-core-skin-title")
    nama_dosen = html_nama_dosen.text
    return nama_dosen

def crawl_deskripsi_dosen(current_search):
    html_deskripsi_dosen = current_search.findAll(class_="gdlr-core-text-box-item-content")
    biografi = html_deskripsi_dosen[0].text
    minat_penelitian = html_deskripsi_dosen[1].text
    return biografi, minat_penelitian

def crawl_url_foto_dosen(current_search):
    html_url_foto = current_search.find(class_="gdlr-core-pbf-element")
    url_foto = html_url_foto.find('a').get('href')
    return url_foto

def updating_general_info_dosen(current_search,id_dosen):
    nama = crawl_nama_dosen(current_search)
    biografi, minat_penelitian = crawl_deskripsi_dosen(current_search)
    url_foto = crawl_url_foto_dosen(current_search)
    email = "gilangcatury@gmail.com"
    with connection.cursor() as cursor:
        cursor.execute("""UPDATE DOSEN SET email=%s, nama_gelar=%s , biografi=%s, minat_penelitian=%s, url_foto=%s WHERE pengguna_id=%s""", [email,nama,biografi,minat_penelitian,url_foto,id_dosen])

def updating_list_pendidikan_and_publikasi_dosen(current_search,id_dosen):
    current_search = current_search.findAll(class_="gdlr-core-pbf-element")
    index_list_pendidikan = 8
    index_list_publikasi = 11
    if len(current_search)>19:
        index_list_pendidikan +=1
        index_list_publikasi += 1
    updating_list_pendidikan(current_search[index_list_pendidikan],id_dosen)
    updating_list_publikasi(current_search[index_list_publikasi],id_dosen)

def updating_list_pendidikan(current_search,id_dosen):
    html_list_pendidikan = current_search.findAll(class_="gdlr-core-icon-list-content")

    no = 1
    for html_pendidikan in html_list_pendidikan :
        pendidikan = html_pendidikan.text
        with connection.cursor() as cursor :
            cursor.execute('''INSERT INTO LIST_PENDIDIKAN_DOSEN VALUES (%s,%s,%s) ON CONFLICT (id_dosen,no) DO UPDATE SET pendidikan = EXCLUDED.pendidikan''',[id_dosen,no,pendidikan]) 
        no += 1

    

def updating_list_publikasi(current_search,id_dosen):
    html_list_publikasi = current_search.findAll(class_="gdlr-core-skin-divider clearfix")
    no= 1
    for html_publikasi in html_list_publikasi:
        html_judul_publikasi = html_publikasi.find(class_="gdlr-core-icon-list-content")
        judul_publikasi = html_judul_publikasi.text
        try :
            url_publikasi = html_publikasi.find("a").get('href')
        except Exception:
            url_publikasi = "#"
        with connection.cursor() as cursor :
            cursor.execute('''INSERT INTO LIST_PUBLIKASI_DOSEN VALUES (%s,%s,%s,%s) ON CONFLICT (id_dosen,no) DO UPDATE SET (no,judul_publikasi,url_publikasi) = (EXCLUDED.no, EXCLUDED.judul_publikasi,EXCLUDED.url_publikasi)''',[id_dosen,no,judul_publikasi,url_publikasi])
        is_publikasi_more_than_3 = no == 3
        if is_publikasi_more_than_3 :
            break

        no += 1
        

def updating_list_url_dosen(current_search,id_dosen):
    html_list_url = current_search.findAll(class_="gdlr-core-button-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align")[0]
    is_list_url_not_empty = len(html_list_url)>0
    if(is_list_url_not_empty):
        list_url = html_list_url.findAll('a')
        url_scopus = list_url[0].get('href')
        url_sinta = list_url[1].get('href')  
        with connection.cursor() as cursor :
            cursor.execute('''INSERT INTO LIST_URL_DOSEN VALUES (%s,'scopus',%s) ON CONFLICT (id_dosen,jenis_url) DO UPDATE SET url = EXCLUDED.url''',[id_dosen,url_scopus])
            cursor.execute('''INSERT INTO LIST_URL_DOSEN VALUES (%s,'sinta',%s) ON CONFLICT (id_dosen,jenis_url) DO UPDATE SET url = EXCLUDED.url''',[id_dosen,url_sinta]) 
                

def updating_bidang_kepakaran_dosen(current_search,id_dosen):
    list_bidang_kepakaran = current_search.find_all(class_="gdlr-core-button-transparent")
    is_list_bidang_kepakaran_not_empty = len(list_bidang_kepakaran)>0
    if(is_list_bidang_kepakaran_not_empty):
        for bidang_kepakaran in list_bidang_kepakaran:
            nama_kepakaran_html = bidang_kepakaran.find(class_="gdlr-core-content")
            nama_kepakaran = nama_kepakaran_html.text
            parameter_nama_kepakaran = "%"+nama_kepakaran+"%"
            with connection.cursor() as cursor:
                    cursor.execute("SELECT id_bidang_kepakaran FROM JENIS_KEPAKARAN WHERE nama LIKE %s",[parameter_nama_kepakaran])
                    print("--")
                    print(parameter_nama_kepakaran)
                    id_kepakaran = namedtuplefetchall(cursor)[0]
                    print(id_kepakaran)
                    print("--")
                    cursor.execute("""INSERT INTO BIDANG_KEPAKARAN_DOSEN VALUES(%s, %s) 
                                        ON CONFLICT (id_bidang_kepakaran, id_dosen) DO NOTHING""", [id_kepakaran, id_dosen])
    return True
def update_cv_mahasiswa(id_mahasiswa, detail_cv):
    with connection.cursor() as cursor:
        tuple_query = [
            detail_cv['cv_url'],
            detail_cv['cv_name'],
            id_mahasiswa
        ]
        cursor.execute("UPDATE SIMKUSPA.MAHASISWA SET cv_url = %s, cv_name = %s WHERE pengguna_id = %s",tuple_query)
        return True

def update_foto_mahasiswa(id_mahasiswa, image_url):
    with connection.cursor() as cursor:
        tuple_query = [
            image_url,
            id_mahasiswa
        ]
        cursor.execute("UPDATE SIMKUSPA.MAHASISWA SET image_url = %s WHERE pengguna_id = %s",tuple_query)
        return True

def update_status_available_dosen(id_dosen, new_status):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE SIMKUSPA.DOSEN SET status = %s WHERE pengguna_id = %s",[new_status,id_dosen])
        return True

def get_all_dosen():
    with connection.cursor() as cursor:
        cursor.execute("""SELECT BK.id_dosen, D.nama_gelar, D.email, D.profile_url FROM DOSEN D JOIN BIDANG_KEPAKARAN_DOSEN BK 
                        ON D.pengguna_id = BK.id_dosen WHERE D.status='Available';""")
        rows = namedtuplefetchall(cursor)
        return rows
