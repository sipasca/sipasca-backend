import json
from django.http import JsonResponse
from django.shortcuts  import render
from .db import add_berkas_penting, hapus_berkas_penting, get_berkas_penting_all, get_berkas_penting_umum, get_berkas_penting_template, get_berkas_penting_lain_lain
from django.views.decorators.http import require_GET, require_POST

# Create your views here.

@require_POST
def create_berkas_penting(request):
    # will be use in integration phase
    if request.method == 'POST':
        body = request.body.decode('utf-8')
        data = json.loads(body)
        url_file = data['url']
        nama_file = data['name']
        jenis_file = data['type']
    add_berkas_penting(url_file,jenis_file,nama_file)
    return JsonResponse({'Success':'Success'})

@require_GET
def delete_berkas_penting(request, delete_id):
    hapus_berkas_penting(delete_id)
    return JsonResponse({'Success':'Berkas berhasil dihapus'})

@require_GET
def berkas_penting_all(request):
    return JsonResponse({ 'berkas_penting_list': get_berkas_penting_all()})

@require_GET
def berkas_penting_umum(request):
    return JsonResponse({ 'berkas_penting_list': get_berkas_penting_umum()})

@require_GET
def berkas_penting_template(request):
    return JsonResponse({ 'berkas_penting_list': get_berkas_penting_template()})

@require_GET
def berkas_penting_lain_lain(request):
    return JsonResponse({ 'berkas_penting_list': get_berkas_penting_lain_lain()})
