from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def add_berkas_penting(url,jenis,nama):
    '''Memasukkan berkas penting ke database'''
    with connection.cursor() as cursor :
        cursor.execute("INSERT INTO BERKAS_PENTING(url,jenis,name) VALUES(%s,%s,%s);",[url,jenis,nama])
        return True

def hapus_berkas_penting(id):
    '''Menghapus berkas penting ke database'''
    with connection.cursor() as cursor :
        cursor.execute("DELETE FROM BERKAS_PENTING WHERE id=%s;",[id])
        return True
 
def get_berkas_penting_all():
    ''' Mengembalikan semua berkas penting dari database'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from berkas_penting ORDER BY id DESC;")
        berkas_penting_all = namedtuplefetchall(cursor)
        return berkas_penting_all

def get_berkas_penting_umum():
    ''' Mengembalikan semua berkas penting umum dari database'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from berkas_penting where jenis='Umum' ORDER BY id DESC;")
        berkas_penting_umum = namedtuplefetchall(cursor)
        return berkas_penting_umum

def get_berkas_penting_template():
    ''' Mengembalikan semua berkas penting template dari database'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from berkas_penting where jenis='Template' ORDER BY id DESC;")
        berkas_penting_template = namedtuplefetchall(cursor)
        return berkas_penting_template

def get_berkas_penting_lain_lain():
    ''' Mengembalikan semua berkas penting lain-lain dari database'''
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from berkas_penting where jenis='Lain-Lain' ORDER BY id DESC;")
        berkas_penting_lain_lain = namedtuplefetchall(cursor)
        return berkas_penting_lain_lain
