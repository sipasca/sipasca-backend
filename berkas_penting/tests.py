import json
from django.test import TestCase
from django.urls import resolve
from .views import delete_berkas_penting

# Create your tests here.
class BerkasPentingTests(TestCase):
    def test_add_berkas_penting_success(self):
        content = {'url':'dummy-url','name':'dummy-name','type':'dummy-type'}
        response = self.client.post('/createBerkasPenting',json.dumps(content), content_type="application/json")
        self.assertEqual(response.status_code, 200)
    
    def test_delete_berkas_penting_success(self):
        response = self.client.get('/deleteBerkasPenting/6/')
        self.assertEqual(response.status_code, 200)
    
    def test_fungsi_delete_berkas_penting_ada(self):
        found = resolve('/deleteBerkasPenting/1/')
        self.assertEqual(found.func, delete_berkas_penting)
        
    def test_berkas_penting_all_successfully(self):
        response = self.client.get('/berkasPentingAll')
        self.assertEqual(response.status_code, 200)

    def test_berkas_penting_umum_successfully(self):
        response = self.client.get('/berkasPentingUmum')
        self.assertEqual(response.status_code, 200)

    def test_berkas_penting_template_successfully(self):
        response = self.client.get('/berkasPentingTemplate')
        self.assertEqual(response.status_code, 200)

    def test_berkas_penting_lain_lain_successfully(self):
        response = self.client.get('/berkasPentingLainlain')
        self.assertEqual(response.status_code, 200)

