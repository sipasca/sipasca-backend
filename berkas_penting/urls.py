from django.urls import path
from . import views

app_name = 'berkas_penting'

urlpatterns = [
    path('createBerkasPenting', views.create_berkas_penting),
    path('deleteBerkasPenting/<str:delete_id>/', views.delete_berkas_penting, name="deleteBerkasPenting"),
    path('berkasPentingAll', views.berkas_penting_all, name="berkasPentingAll"),
    path('berkasPentingUmum', views.berkas_penting_umum, name="berkasPentingUmum"),
    path('berkasPentingTemplate', views.berkas_penting_template, name="berkasPentingYemplate"),
    path('berkasPentingLainlain', views.berkas_penting_lain_lain, name="berkasPentingLainlain"),
]
