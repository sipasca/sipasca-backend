from django.apps import AppConfig


class BerkasPentingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'berkas_penting'
