from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
    
def get_topik_mahasiswa(mk_id, mahasiswa_id):
    '''Mengembalikan topik mahasiswa'''
    with connection.cursor() as cursor :
        cursor.execute(
            """SELECT t.id, t.judul, t.deskripsi, t.batas_waktu, d.motivasi FROM MK_MAHASISWA m, TOPIK_MAHASISWA t, MK_MAHASISWA_DOSEN d WHERE m.id_mk=d.id_mk AND m.id_mahasiswa = d.id_mahasiswa AND m.id_topik_mahasiswa = t.id AND m.id_mk =%s AND m.id_mahasiswa = %s ;""",
            [mk_id, mahasiswa_id])
        result = namedtuplefetchall(cursor)
        return result

def create_topik_dosen(detail_topik,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3):
    '''Membuat topik dosen'''
    with connection.cursor() as cursor:
        query_detail_topik = [
            detail_topik['id_dosen'],
            detail_topik['judul'],
            detail_topik['deskripsi'],
            detail_topik['prasyarat_1'],
            detail_topik['prasyarat_2'],
            detail_topik['prasyarat_3'],
            'belum diambil',
            detail_topik['batas_diterima']
        ]
        cursor.execute("insert into SIMKUSPA.topik_dosen(id_dosen,judul,deskripsi,prasyarat_1,prasyarat_2,prasyarat_3,status,batas_diterima,jml_diterima) values(%s,%s,%s,%s,%s,%s,%s,%s,0)",query_detail_topik)
        
        cursor.execute("SELECT MAX(id) AS maxid from SIMKUSPA.topik_dosen")

        id_topik = namedtuplefetchall(cursor)[0][0]
        create_dokumen_topik(id_topik,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3)
        return id_topik

def create_dokumen_topik(id_topik,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3):
    '''Create dokumen topik'''
    with connection.cursor() as cursor:
        query = "insert into SIMKUSPA.dokumen_topik (id_topik,url,nama_file,no_dokumen) values(%s,%s,%s,%s)"
        url = 'production/dokumenTopik/' + str(id_topik) +'/'
        if detail_dokumen_1 != "":
            detail_dokumen_1_url = url + 'dokumen_1'
            query_detail_dokumen_1 = [
            id_topik,
            detail_dokumen_1_url,
            detail_dokumen_1['nama_file'],
            1
            ]
            cursor.execute(query,query_detail_dokumen_1)

        if detail_dokumen_2 != "":
            detail_dokumen_2_url = url + 'dokumen_2'
            query_detail_dokumen_2 = [
            id_topik,
            detail_dokumen_2_url,
            detail_dokumen_2['nama_file'],
            2
            ]
            cursor.execute(query,query_detail_dokumen_2)

        if detail_dokumen_3 != "":
            detail_dokumen_3_url = url + 'dokumen_3'
            query_detail_dokumen_3 = [
            id_topik,
            detail_dokumen_3_url,
            detail_dokumen_3['nama_file'],
            3
            ]
            cursor.execute(query,query_detail_dokumen_3)

        return True
        
def update_topik_dosen(id,detail_topik,deleted_file,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3):
    '''Update topik dosen'''
    with connection.cursor() as cursor:
        query_detail_topik = [
            detail_topik['judul'],
            detail_topik['deskripsi'],
            detail_topik['prasyarat_1'],
            detail_topik['prasyarat_2'],
            detail_topik['prasyarat_3'],
            detail_topik['batas_diterima'],
            id
        ]
        
        cursor.execute("UPDATE SIMKUSPA.topik_dosen SET judul = %s, deskripsi = %s, prasyarat_1 = %s, prasyarat_2=%s, prasyarat_3=%s, batas_diterima=%s WHERE id = %s",query_detail_topik)

        for no_dokumen in deleted_file:
            cursor.execute("DELETE FROM SIMKUSPA.dokumen_topik where no_dokumen = %s AND id_topik = %s",[no_dokumen,id])

        create_dokumen_topik(id,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3)
        return True

def get_all_topik_dosen():
    '''Mengembalikan daftar seluruh topik yang belum diambil di topik dosen'''
    with connection.cursor() as cursor:
        cursor.execute("""(SELECT T.id, T.judul, D.nama_gelar, T.deskripsi, T.batas_diterima, T.jml_diterima
            FROM simkuspa.topik_dosen T LEFT JOIN simkuspa.dosen D 
            ON T.id_dosen = D.pengguna_id WHERE T.status ='belum diambil' 
            AND T.jml_diterima < T.batas_diterima ORDER BY T.id DESC)
            UNION ALL
            (SELECT T.id, T.judul, D.nama_gelar, T.deskripsi, T.batas_diterima, T.jml_diterima
            FROM simkuspa.topik_dosen T LEFT JOIN simkuspa.dosen D 
            ON T.id_dosen = D.pengguna_id WHERE T.status ='belum diambil' 
            AND T.batas_diterima = -1 ORDER BY T.id DESC);
            """)
        daftar_topik_dosen = namedtuplefetchall(cursor)
        return daftar_topik_dosen

def get_list_topik_by_dosen(dosen_id):
    '''Mengembalikan daftar topik yang dibuat oleh seorang dosen'''
    with connection.cursor() as cursor:
        cursor.execute("""SELECT T.id, T.judul, D.nama_gelar, T.deskripsi, T.batas_diterima, T.jml_diterima
            FROM simkuspa.topik_dosen T LEFT JOIN simkuspa.dosen D 
            ON T.id_dosen = D.pengguna_id where T.id_dosen=%s ORDER BY T.id DESC;
            """, [dosen_id])
        daftar_topik_by_dosen_id = namedtuplefetchall(cursor)
        return daftar_topik_by_dosen_id

def get_topik_dosen_by_id(topik_id):
    '''Mengembalikan topik dosen berdasarkan ID topik'''
    with connection.cursor() as cursor :
        cursor.execute("""SELECT T.id, T.id_dosen, T.judul, T.deskripsi, T.prasyarat_1, T.prasyarat_2,
            T.prasyarat_3, T.status, T.batas_diterima, T.jml_diterima, D.nama_gelar
            FROM simkuspa.topik_dosen T, simkuspa.dosen D 
            WHERE T.id=%s and D.pengguna_id = T.id_dosen""", [topik_id])
        result = namedtuplefetchall(cursor)
        cursor.execute("SELECT url, nama_file, no_dokumen FROM simkuspa.dokumen_topik WHERE id_topik=%s order by no_dokumen", [topik_id])
        result.append(namedtuplefetchall(cursor))
        return result
        
def delete_topik_dosen_by_id(topik_id):
    '''Menghapus sebuah topik yang dimiliki oleh seorang dosen'''
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM TOPIK_DOSEN WHERE id = %s",[topik_id])
        return True

def update_topik_mahasiswa(id_topik, id_mahasiswa, judul, deskripsi, batas_waktu):
    '''Update topik yang dimiliki mahasiswa pada suatu mata kuliah'''
    with connection.cursor() as cursor:
        query_detail_topik = [
            judul, deskripsi, batas_waktu, id_topik, id_mahasiswa, 
        ]
        cursor.execute("""UPDATE SIMKUSPA.topik_mahasiswa SET judul = %s, deskripsi = %s, batas_waktu = %s  
                        WHERE id = %s AND mahasiswa_id=%s""",query_detail_topik)
        return True