from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse
import json
from topik.db import get_topik_mahasiswa, create_topik_dosen, update_topik_dosen, get_all_topik_dosen, get_list_topik_by_dosen, delete_topik_dosen_by_id, get_topik_dosen_by_id, update_topik_mahasiswa

# Create your views here.
@require_GET
def topik_mahasiswa(request, mk_id, mahasiswa_id):
    context = {
        'topik_mahasiswa' : get_topik_mahasiswa(mk_id, mahasiswa_id)
    }
    return JsonResponse(context)

@require_POST
def buat_topik_dosen(request):
    data = json.loads(request.body)
    id_dosen = data['id_dosen']
    judul = data['judul']
    deskripsi = data['deskripsi']
    prasyarat_1 = data['prasyarat_1']
    prasyarat_2 = data['prasyarat_2']
    prasyarat_3 = data['prasyarat_3']
    dokumen_1 = data['dokumen_1']
    dokumen_2 = data['dokumen_2']
    dokumen_3 = data['dokumen_3']
    batas_diterima = data['batas_diterima']

    detail_topik = {
        'id_dosen' : id_dosen,
        'judul' : judul,
        'deskripsi' : deskripsi,
        'prasyarat_1' : prasyarat_1,
        'prasyarat_2' : prasyarat_2,
        'prasyarat_3' : prasyarat_3,
        'batas_diterima' : batas_diterima,
    }
    if dokumen_1 != "" :
        detail_dokumen_1 = {
        'nama_file': dokumen_1['nama_file'],
        }
    else :
        detail_dokumen_1 = ""

    if dokumen_2 != "" :
        detail_dokumen_2 = {
        'nama_file': dokumen_2['nama_file'],
        }
    else:
        detail_dokumen_2 = ""

    if dokumen_3 != "" :
        detail_dokumen_3 = {
            'nama_file': dokumen_3['nama_file'],
        }
    else:
        detail_dokumen_3 = ""

    id_topik = create_topik_dosen(detail_topik,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3)

    return JsonResponse({'status':'Success', 'id_topik':id_topik})

@require_POST
def edit_topik_dosen(request,topik_id):
    new_data = json.loads(request.body)
    
    judul = new_data['judul']
    deskripsi = new_data['deskripsi']
    prasyarat_1 = new_data['prasyarat_1']
    prasyarat_2 = new_data['prasyarat_2']
    prasyarat_3 = new_data['prasyarat_3']
    dokumen_1 = new_data['dokumen_1']
    dokumen_2 = new_data['dokumen_2']
    dokumen_3 = new_data['dokumen_3']
    deleted_file = new_data['deleted_file']
    batas_diterima = new_data['batas_diterima']

    new_detail_topik = {
        'judul' : judul,
        'deskripsi' : deskripsi,
        'prasyarat_1' : prasyarat_1,
        'prasyarat_2' : prasyarat_2,
        'prasyarat_3' : prasyarat_3,
        'batas_diterima' : batas_diterima,
    }

    if dokumen_1 != "" :
        detail_dokumen_1 = {
        'url': dokumen_1['url'],
        'nama_file': dokumen_1['nama_file'],
        }
    else :
        detail_dokumen_1 = ""

    if dokumen_2 != "" :
        detail_dokumen_2 = {
        'url': dokumen_2['url'],
        'nama_file': dokumen_2['nama_file'],
        }
    else:
        detail_dokumen_2 = ""

    if dokumen_3 != "" :
        detail_dokumen_3 = {
            'url': dokumen_3['url'],
            'nama_file': dokumen_3['nama_file'],
        }
    else:
        detail_dokumen_3 = ""

    update_topik_dosen(topik_id,new_detail_topik,deleted_file,detail_dokumen_1,detail_dokumen_2,detail_dokumen_3)
    return JsonResponse({'status':'Success'})

@require_GET
def daftar_topik(request):
    return JsonResponse({ 'daftar_topik_list': get_all_topik_dosen()})

@require_GET
def daftar_topik_by_dosen(request,dosen_id):
    return JsonResponse({ 'list_topik_by_dosen': get_list_topik_by_dosen(dosen_id)})

@require_GET
def delete_topik_dosen(request, topik_id):
    delete_topik_dosen_by_id(topik_id)
    return JsonResponse({'Success':'Success'})

@require_GET
def detail_topik_dosen(request, topik_id):
    context = {
        'detail_topik_dosen' : get_topik_dosen_by_id(topik_id)
    }
    return JsonResponse(context)

@require_POST
def edit_topik_mahasiswa(request):
    data = json.loads(request.body)
    id_topik = data['id_topik']
    id_mahasiswa = data['id_mahasiswa']
    judul = data['judul']
    deskripsi = data['deskripsi']
    batas_waktu = data['batas_waktu']

    update_topik_mahasiswa(id_topik, id_mahasiswa, judul, deskripsi, batas_waktu)

    context = {
        'status': 'SUCCESS'
    }

    return JsonResponse(context)
