from django.test import TestCase
import json

# Create your tests here.
type = "application/json"
class TopikTests(TestCase):
    def test_get_topik_mahasiswa_succesfully(self):
        response = self.client.get('/topikMahasiswa/CSGE802097/M1234567890/')
        self.assertEqual(response.status_code, 200)

    def test_buat_topik_dosen_successfully(self):
        detail_topik = {
        'id_dosen' : 'D1234567890',
        'judul' : 'judul',
        'deskripsi' : 'deskripsi',
        'prasyarat_1' : 'prasyarat1',
        'prasyarat_2' : 'prasyarat2',
        'prasyarat_3' : 'prasyarat3',
        'batas_diterima' : 1,
        'dokumen_1' :  {
            'nama_file': 'nama_file'
        } ,
        'dokumen_2' :  {
            'nama_file': 'nama_file2'
        },
        'dokumen_3' :  {
            'nama_file': 'nama_file3'
        }
        }
        response = self.client.post('/buatTopikDosen/',json.dumps(detail_topik), content_type = type)
        self.assertEqual(response.status_code, 200)

    def test_buat_topik_dosen_with_empty_dokumen_successfully(self):
        detail_topik = {
        'id_dosen' : 'D1234567890',
        'judul' : 'judul2',
        'deskripsi' : 'deskripsi2',
        'prasyarat_1' : 'prasyarat1',
        'prasyarat_2' : 'prasyarat2',
        'prasyarat_3' : 'prasyarat3',
        'batas_diterima' : -1,
        'dokumen_1' :  '',
        'dokumen_2' : '',
        'dokumen_3' : ''
        }
        response = self.client.post('/buatTopikDosen/',json.dumps(detail_topik), content_type = type)
        self.assertEqual(response.status_code, 200)

    def test_edit_topik_dosen_successfully(self):
        detail_topik = {
        'judul' : 'judul baru',
        'deskripsi' : 'deskripsi baru',
        'prasyarat_1' : 'prasyarat1 baru',
        'prasyarat_2' : 'prasyarat2 baru',
        'prasyarat_3' : 'prasyarat3 baru',
        'batas_diterima' : 1,
        'deleted_file':[1],
        'dokumen_1' :  {
            'url' : 'url1',
            'nama_file': 'nama_file'
        } ,
        'dokumen_2' :  {
            'url' : 'url2',
            'nama_file': 'nama_file2'
        },
        'dokumen_3' :  {
            'url' : 'url3',
            'nama_file': 'nama_file3'
        }
        }
        response = self.client.post('/editTopikDosen/28/',json.dumps(detail_topik), content_type = type)
        self.assertEqual(response.status_code, 200)
    
    def test_edit_topik_dosen_with_empty_dokumen_successfully(self):
        detail_topik = {
        'judul' : 'judul baru',
        'deskripsi' : 'deskripsi baru',
        'prasyarat_1' : 'prasyarat1 baru',
        'prasyarat_2' : 'prasyarat2 baru',
        'prasyarat_3' : 'prasyarat3 baru',
        'batas_diterima' : 1,
        'deleted_file':[],
        'dokumen_1' :  '',
        'dokumen_2' :  '',
        'dokumen_3' :  ''
        }
        response = self.client.post('/editTopikDosen/28/',json.dumps(detail_topik), content_type = type)
        self.assertEqual(response.status_code, 200)

    def test_get_all_topik_dosen_successfully(self):
        response = self.client.get('/daftarTopik')
        self.assertEqual(response.status_code, 200)

    def test_get_list_topik_by_dosen_id_successfully(self):
        response = self.client.get('/daftarTopikByDosen/D1234567890/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_topik_dosen_by_id_successfully(self):
        response = self.client.get('/detailTopikDosen/1/')
        self.assertEqual(response.status_code, 200)
    
    def test_get_topik_dosen_by_id_unsuccessfully(self):
        response = self.client.get('/detailTopikDosen/')
        self.assertEqual(response.status_code, 404)

    def test_delete_topik_dosen(self):
        response = self.client.get("/deleteTopikDosen/0/")
        self.assertEqual(response.status_code, 200)
    
    def test_edit_topik_mahasiswa_successfully(self):
        detail_topik = {
            'judul' : 'Machine Learning',
            'deskripsi' : 'Deskripsi Machine Learning',
            'batas_waktu' : '2023-01-15',
            'id_topik' : '1',
            'id_mahasiswa' : 'M9636831678',
        }
        response = self.client.post('/editTopikMahasiswa/',json.dumps(detail_topik), content_type = type)
        self.assertEqual(response.status_code, 200)
