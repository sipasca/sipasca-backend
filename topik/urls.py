from django.urls import path
from . import views

app_name = 'topik'

urlpatterns = [
    path('topikMahasiswa/<str:mk_id>/<str:mahasiswa_id>/', views.topik_mahasiswa, name='topikMahasiswa'),
    path('editTopikMahasiswa/', views.edit_topik_mahasiswa, name='editTopikMahasiswa'),
    path('buatTopikDosen/', views.buat_topik_dosen, name="buatTopikDosen"),
    path('editTopikDosen/<str:topik_id>/', views.edit_topik_dosen, name="editTopikDosen"),
    path('detailTopikDosen/<str:topik_id>/', views.detail_topik_dosen, name='detailTopikDosen'),
    path('daftarTopik', views.daftar_topik, name='daftarTopik'),
    path('daftarTopikByDosen/<str:dosen_id>/', views.daftar_topik_by_dosen, name='daftarTopikByDosen'),
    path('deleteTopikDosen/<str:topik_id>/',views.delete_topik_dosen, name="deleteTopikDosen")
]