from django.test import TestCase
from django.test import SimpleTestCase

# Create your tests here.
class HelloTests(SimpleTestCase):
    
    def test_hello_running_successfully(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)