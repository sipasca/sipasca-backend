from django.test import TestCase
from django.test import SimpleTestCase
import json
send_type = "application/json"
url = '/editPengumuman/5'
url_buat = "/buatPengumuman"
# Create your tests here.
class PengumumanTests(TestCase):
# Create your tests here.
    def test_pengumuman_all_successfully(self):
        response = self.client.get('/pengumumanAll')
        self.assertEqual(response.status_code, 200)

    def test_pengumuman_Detail_successfully(self):
        response = self.client.get('/pengumumanDetail/5/')
        self.assertEqual(response.status_code, 200)

    def test_buat_pengumuman_successfully(self):
        data = {
         'judul' : 'judul',
         'konten' : 'konten'}
        response = self.client.post(url_buat, json.dumps(data), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
        self.assertEqual(response.status_code, 200)

    def test_update_pengumuman_successfully(self):
        data = {
         'judul' : 'judul',
         'konten' : 'konten'}
        response = self.client.post(url, json.dumps(data), content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_delete_pengumuman_running_successfully(self):
        response = self.client.get('/hapusPengumuman/10')
        self.assertEqual(response.status_code, 200)

