from django.db import connection
from collections import namedtuple

from django.http import request


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def get_pengumuman_all():
    ''' Mengembalikan judul, tanggal, konten pengumuman'''
    with connection.cursor() as cursor:
        cursor.execute(
        """SELECT * from pengumuman ORDER BY lastmodified DESC;
        """)
        rows = namedtuplefetchall(cursor)
        return rows

def get_pengumuman_detail(pengumuman_id):
    ''' Mengembalikan judul, tanggal, konten pengumuman'''
    with connection.cursor() as cursor :
        cursor.execute(
        """ SELECT * from pengumuman
        WHERE id = %s;
        """,[pengumuman_id]
        )
        rows = namedtuplefetchall(cursor)
        return rows

def create_pengumuman(dict_detail):
    ''' menyimpan pengumuman baru dalam pengumuman'''
    with connection.cursor() as cursor:
        tuple_query = [
            dict_detail['pengumumanJudul'],
            dict_detail['pengumumanKonten'],
            dict_detail['pengumumanTanggal'],
            dict_detail['lastmodified']
        ]
        cursor.execute("insert into SIMKUSPA.pengumuman(judul,konten,tanggal,lastmodified) values(%s, %s,%s, %s)",tuple_query)
        return True

def update_pengumuman(dict_detail):
    with connection.cursor() as cursor:
        tuple_query = [
            dict_detail['pengumumanJudul'],
            dict_detail['pengumumanKonten'],
            dict_detail['pengumumanTanggal'],
            dict_detail['lastmodified'],
            dict_detail['pengumumanId'],
            
        ]
        cursor.execute("UPDATE SIMKUSPA.PENGUMUMAN SET judul = %s, konten = %s, tanggal = %s, lastmodified = %s WHERE id = %s ",tuple_query)
        return True

def delete_pengumuman(hapus_id):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM SIMKUSPA.PENGUMUMAN WHERE id=%s",[hapus_id])
        return True
        
