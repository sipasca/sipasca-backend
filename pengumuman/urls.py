from django.urls import path
from . import views

app_name = 'pengumuman'

urlpatterns = [
    path('pengumumanAll', views.pengumuman_all, name="pengumumanAll"),
    path('pengumumanDetail/<str:pengumuman_id>/', views.pengumuman_detail, name="pengumumanDetail"),
    path('buatPengumuman', views.buat_pengumuman, name="buat_pengumuman"),
    path('editPengumuman/<str:edit_id>', views.edit_pengumuman, name="edit_pengumuman"),
    path('hapusPengumuman/<str:hapus_id>', views.hapus_pengumuman, name="hapus_pengumuman")
]
