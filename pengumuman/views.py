from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_GET, require_POST
from django.http import JsonResponse
from django.db import connection
from django.contrib import messages
from .db import get_pengumuman_all, get_pengumuman_detail,create_pengumuman,update_pengumuman,delete_pengumuman
import json
import datetime



@require_GET
def pengumuman_all(request):
    context = { 
        'pengumuman_list': get_pengumuman_all()
    }
    
    return JsonResponse(context)

@require_GET
def pengumuman_detail(request, pengumuman_id):
    
    context = {
        'pengumuman_detail' : get_pengumuman_detail(pengumuman_id)
    }
    
    return JsonResponse(context)

@require_POST
def buat_pengumuman(request):
    currentdate = datetime.date.today()
    now = datetime.datetime.now()
    tanggal = currentdate.strftime("%m-%d-%Y")

    data = json.loads(request.body)
    judul= data["judul"]
    konten = data["konten"]

    status = 'SUCCESS'

    dict_detail = {
            'pengumumanJudul': judul,
            'pengumumanKonten' : konten,
            'pengumumanTanggal': tanggal,
            'lastmodified' : now,
    }

    create_pengumuman(dict_detail)

    context = {
         'status' : status
    }
    return JsonResponse(context)

@require_POST
def edit_pengumuman(request,edit_id):   
    ''' mengubah pengumuman yang terdaftar dalam pengumuman'''
    currentdate = datetime.date.today()
    now = datetime.datetime.now()
    tanggal = currentdate.strftime("%m-%d-%Y")
    data = json.loads(request.body)
    judul= data["judul"]
    konten = data["konten"]

    pengumuman_id = edit_id
    
    dict_detail = {
            'pengumumanId': pengumuman_id ,
            'pengumumanJudul': judul,
            'pengumumanKonten' : konten,
            'pengumumanTanggal':tanggal,
            'lastmodified' : now,
    }

    update_pengumuman(dict_detail)

    context = {
        'Success':'Pengumuman Berhasil Diedit'
    }
    return JsonResponse(context)

@require_GET
def hapus_pengumuman(request,hapus_id):   
    ''' menghapus pengumuman dengan id tertentu dalam tabel pengumuman'''

    delete_pengumuman(hapus_id)

    status = 'SUCCESS'

    context = {
        'status' : status
    }
    return JsonResponse(context)



