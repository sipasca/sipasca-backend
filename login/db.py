from django.db import connection

def login_verification(username, password):
    result = None
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM PENGGUNA WHERE username=%s AND password=%s", [username, password])
        result = cursor.fetchone()
    return result

def get_list_id():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM PENGGUNA")
        result = cursor.fetchall()

        existing_ids = []
        for user_id in result:
            existing_ids.append(user_id[0])
        return existing_ids
    
def get_id_by_username(username):
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM PENGGUNA WHERE username=%s",[username])
        result = cursor.fetchone()
        return result

def insert_into_pengguna(user_id, username):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO PENGGUNA(id, username) VALUES(%s, %s)",[user_id, username])
        return True

def insert_into_dosen(user_id, name, name_with_title, nip,profile_url):
    with connection.cursor() as cursor:
        cursor.execute("""INSERT INTO DOSEN(pengguna_id, nama, nama_gelar, nip, profile_url) 
                        VALUES(%s, %s, %s, %s, %s)""",[user_id, name, name_with_title, nip,profile_url])
        return True

def insert_into_mahasiswa(user_id, name, name_with_title, email, prodi, npm):
    with connection.cursor() as cursor:
        cursor.execute("""INSERT INTO MAHASISWA(pengguna_id, nama, nama_gelar, email, prodi, npm) 
                        VALUES(%s, %s, %s, %s, %s, %s)""",[user_id, name, name_with_title,email, prodi, npm])
        return True