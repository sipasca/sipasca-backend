from django.test import TestCase
from django.test import SimpleTestCase
from unittest import mock
from django.test.runner import DiscoverRunner
from unittest import mock
from .static.test import html_content
from profiles.tests import mocked_requests_get_special_case
import json

send_type = "application/json"
url = '/login'
url_sso = '/loginSSO/'

sso = 'SSO'
cas_sr = 'cas:serviceResponse'
cas_as = 'cas:authenticationSuccess'
cas_user = 'cas:user'
cas_attr = 'cas:attributes'
cas_nama = 'cas:nama'
cas_ld = 'cas:ldap_cn'

mahasiswa = {
            sso: {
                cas_sr: {
                    cas_as: {
                        cas_user: 'user.mahasiswa',
                        cas_attr: {
                            cas_nama: 'User Mahasiswa',
                            cas_ld: 'User Mahasiswa',
                            'cas:npm': '9999888877',
                            'cas:kd_org': '09.00.12.01' #NOSONAR
                        }
                    }
                }
            }
        }

mahasiswa2 = {
            sso: {
                cas_sr: {
                    cas_as: {
                        cas_user: 'user.mahasiswa2',
                        cas_attr: {
                            cas_nama: 'User Mahasiswa 2',
                            cas_ld: 'User Mahasiswa 2',
                            'cas:npm': '1112223334',
                            'cas:kd_org': '06.00.12.01' #NOSONAR
                        }
                    }
                }
            }
        }

dosen = {
        sso: {
            cas_sr: {
                cas_as: {
                    cas_user: 'chan',
                    cas_attr: {
                        cas_nama: 'User Dosen',
                        cas_ld: 'User Dosen',
                        'cas:nip': '99998888771122334'
                    }
                }
            }
        }
    }


a = ['a' for _ in range(20)]
b = ['b' for _ in range(10)]
mocked_choice = a + b

def mocked_requests_get_profile_url(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code
            self.content = bytes(html_content, 'utf-8')

    return MockResponse({"content": "value2"}, 200)

class LoginTests(TestCase):

    def test_login_running_successfully(self):
        data = {'username': 'mahasiswa1', 'password': 'password1'}
        response = self.client.post(url, json.dumps(data), content_type=send_type)
        self.assertEqual(response.status_code, 200)

    def test_login_mahasiswa_success(self):
        dosen = {'username': 'mahasiswa1', 'password': 'password1'}
        response = self.client.post(url, json.dumps(dosen), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')

    def test_login_dosen_success(self):
        dosen = {'username': 'dosen1', 'password': 'password1'}
        response = self.client.post(url, json.dumps(dosen), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    def test_login_staf_success(self):
        dosen = {'username': 'staf1', 'password': 'password1'}
        response = self.client.post(url, json.dumps(dosen), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    def test_login_unrecognized(self):
        wrong_user = {'username': 'wrong user', 'password': 'wrong password'}
        response = self.client.post(url, json.dumps(wrong_user), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'UNRECOGNIZED')
    
    def test_login_first_time_sso_mahasiswa(self):
        response = self.client.post(url_sso, json.dumps(mahasiswa), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    @mock.patch('requests.get', side_effect=mocked_requests_get_profile_url)
    @mock.patch('requests.get', side_effect=mocked_requests_get_special_case)
    def test_login_first_time_sso_dosen(self,mock_requests_get_profile_url, mock_requests_get_special_case):
        response = self.client.post(url_sso, json.dumps(dosen), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    def test_existing_user_login_sso(self):
        self.client.post(url_sso, json.dumps(mahasiswa), content_type=send_type)
        response = self.client.post(url_sso, json.dumps(mahasiswa), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
    
    @mock.patch('secrets.choice', side_effect=mocked_choice)
    def test_id_generate_same(self, mock_secret_choice):
        self.client.post(url_sso, json.dumps(mahasiswa), content_type=send_type)
        response = self.client.post(url_sso, json.dumps(mahasiswa2), content_type=send_type)
        response_data = response.json()
        self.assertEqual(response_data.get('status'), 'SUCCESS')
